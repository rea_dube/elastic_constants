import ase
from ase.calculators.dftb import Dftb
from ase.optimize import  QuasiNewton, BFGS, FIRE
from ase.constraints import ExpCellFilter
from ase.optimize.precon import Exp, PreconLBFGS, PreconFIRE
from ase.io import write, read, Trajectory
from ase.calculators.calculator import kptdensity2monkhorstpack
import ase.spacegroup
import scipy as sci
import re
import numpy as np
import pylab as P

def main():
    #traj = Trajectory('ammonia_DFTB_MBD.traj')
    cif_file = 'ammonia.cif' #input("Enter cif file here:")
    crystal = read(cif_file) #traj[-1]
    seedname = 'ammonia_DFTB_MBD-15_222'#input("Enter seedname here:")
    units = 'GPa'
    cell = crystal.get_cell()
    sg = ase.spacegroup.get_spacegroup(crystal)
    spacegroup = sg.no
    kpts = kptdensity2monkhorstpack(crystal, kptdensity=3.0, even=False)
    offset = []
    kptstr = "SupercellFolding {" + str(kpts[0]) + " 0 0 0 " + str(kpts[1]) + " 0 0 0 " + str(kpts[2])
    for kpt in kpts:
    	if kpt % 2 == 0:
            offset.append(0.5)
    	else:
   	    offset.append(0.0)
    offsetstr = " " + str(offset[0]) + " " + str(offset[1]) + " " + str(offset[2]) + " }"

    crystal.set_calculator(Dftb(label=seedname,
                                atoms=None,
                                kpts=None,
                                Hamiltonian_SCC="Yes",
                                Hamiltonian_DampXH="Yes",
				Hamiltonian_DampXHExponent = 4,
                                Hamiltonian_SCCTolerance = 1.00E-10,
                                Hamiltonian_Dispersion_ = "MBD",
                                Hamiltonian_Dispersion_beta = 0.86,
                                Hamiltonian_Dispersion_NoMegaGrid = 15,
                                Hamiltonian_Dispersion_KGrid = "1 1 1",
                                # Hamiltonian_Dispersion_KGridShift = "0.5 0.5 0.5",
                                Hamiltonian_Dispersion_VacuumAxis = "No No No",
                                Hamiltonian_KPointsAndWeights=kptstr + offsetstr,
                                Hamiltonian_ThirdOrderFull='Yes',
                                Hamiltonian_HubbardDerivs_='',
                                Hamiltonian_HubbardDerivs_C=-0.1492,
                                Hamiltonian_HubbardDerivs_H=-0.1857,
                                Hamiltonian_HubbardDerivs_N=-0.1535,
                                Hamiltonian_HubbardDerivs_O=-0.1575,
                                Hamiltonian_HubbardDerivs_S=-0.1100,
                                Hamiltonian_MaxAngularMomentum_='',
                                Hamiltonian_MaxAngularMomentum_O='"p"',
                                Hamiltonian_MaxAngularMomentum_H='"s"',
                                Hamiltonian_MaxAngularMomentum_C='"p"',
                                Hamiltonian_MaxAngularMomentum_N='"p"',
                                Hamiltonian_MaxAngularMomentum_S='"d"'
                                ))
    ucf = ExpCellFilter(crystal)
    dyn = QuasiNewton(ucf)
    traj = Trajectory(seedname+'.traj', 'w', crystal)
    dyn.attach(traj)
    dyn.run(fmax=0.001)
    #dyn = FIRE(crystal)
    #dyn.run(fmax=0.005)
    stress = crystal.get_stress() / ase.units.GPa
    write('opt_' + seedname + '.cif', crystal) 

if __name__ == "__main__":
    main()


