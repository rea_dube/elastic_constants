#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Import various parts of ASE - not all are used (at this stage)

import ase
import os
import sys
import os.path
import subprocess
#os.system('/home/rzwane/Documents/DFTB/MBD/aspirin_ii/ase/dftb_command.sh')
#subprocess.call("/home/rzwane/Documents/DFTB/MBD/aspirin_ii/ase/dftb_command.sh")
#get_ipython().run_line_magic('env', 'DFTB_COMMAND=mpiexec -n 6 /home/rzwane/Downloads/dftbplus/_install/bin/dftb+')
#os.environ['DFTB_COMMAND']="mpiexec -n 2 /home/rzwane/Downloads/dftbplus/_install/bin/dftb+"
from ase.calculators.dftb import Dftb
from ase.optimize import  QuasiNewton, BFGS, FIRE
from ase.constraints import ExpCellFilter
from ase.optimize.precon import Exp, PreconLBFGS, PreconFIRE
from ase.io import write, read, Trajectory
from ase.calculators.calculator import kptdensity2monkhorstpack
print(os.getcwd())


# In[2]:


seedname = 'BENZEN20_DFTB_MBD'
# Import your crystal structure - change name to a file in the current folder (alongside the script)
atoms = read('BENZEN20.cif')

# Determine k-point grid
kpts = kptdensity2monkhorstpack(atoms,kptdensity=3.0,even=False)
offset = []
kptstr = "SupercellFolding {"+str(kpts[0])+" 0 0 0 "+str(kpts[1])+" 0 0 0 "+str(kpts[2])
for kpt in kpts:
    if kpt %2 == 0:
        offset.append(0.5)
    else:
        offset.append(0.0)
offsetstr = " "+str(offset[0])+" "+str(offset[1])+" "+str(offset[2])+" }"
print(kpts)


# In[3]:


# mbd
MBD = Dftb(label=seedname,
                         atoms=atoms,
                         kpts=None,
                         Hamiltonian_SCC = "Yes",
                         Hamiltonian_SCCTolerance = 1.00E-10,
                         #Driver_='ConjugateGradient',
                         #Driver_MaxForceComponent='1E-4',
                         #Driver_MaxSteps=1000,
                         Hamiltonian_Dispersion_ = "MBD",
                         Hamiltonian_Dispersion_beta = 0.86,
                         Hamiltonian_Dispersion_NoMegaGrid = 15,
                         Hamiltonian_Dispersion_KGrid = "2 2 2",
                         Hamiltonian_Dispersion_KGridShift = "0.5 0.5 0.5",
                         Hamiltonian_Dispersion_VacuumAxis = "No No No",
                         #Hamiltonian_Dispersion_ReferenceSet = "TS",
                         Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                         Hamiltonian_ThirdOrderFull = 'Yes',          
                         Hamiltonian_HubbardDerivs_= '',
                         Hamiltonian_HubbardDerivs_C = -0.1492,
                         Hamiltonian_HubbardDerivs_H = -0.1857,
                         Hamiltonian_HubbardDerivs_N = -0.1535,
                         Hamiltonian_HubbardDerivs_O = -0.1575,
                         Hamiltonian_HubbardDerivs_S = -0.0695,
                         Hamiltonian_HubbardDerivs_P = -0.0702,
                         Hamiltonian_MaxAngularMomentum_='',
                         Hamiltonian_MaxAngularMomentum_O='"p"',
                         Hamiltonian_MaxAngularMomentum_H='"s"',
                         Hamiltonian_MaxAngularMomentum_C='"p"',
                         Hamiltonian_MaxAngularMomentum_N='"p"',
                         Hamiltonian_MaxAngularMomentum_P='"d"',
                         Hamiltonian_MaxAngularMomentum_S='"d"' 
                        )


# In[4]:


# DFTB3-D3 Boese (https://www.sciencedirect.com/science/article/pii/S0009261419300521) 
# Hubbard values from dftb.org
# (note difference for P and S from https://pubs.acs.org/doi/pdf/10.1021/ct401002w)
DFTB3D3B = Dftb(label=seedname,
                         atoms=atoms,
                         kpts=None,
                         Hamiltonian_SCC = "Yes",
                         Hamiltonian_SCCTolerance = 1.00E-10,
                         Hamiltonian_Dispersion_ = "DftD3",
                         Hamiltonian_Dispersion_Damping_ = 'BeckeJohnson',
                         Hamiltonian_Dispersion_Damping_a1 = 1.3719,
                         Hamiltonian_Dispersion_Damping_a2 = 3.7017,
                         Hamiltonian_Dispersion_s6 = 1.0,
                         Hamiltonian_Dispersion_s8 = 0.5883,
                         Hamiltonian_Dispersion_Threebody = "No",
                         Hamiltonian_Dispersion_HHRepulsion = "No",
                         Hamiltonian_HCorrection_ = 'Damping',
                         Hamiltonian_HCorrection_Exponent = 4.2,  
                         Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                         Hamiltonian_ThirdOrderFull = 'Yes',          
                         Hamiltonian_HubbardDerivs_= '',
                         Hamiltonian_HubbardDerivs_C = -0.1492,
                         Hamiltonian_HubbardDerivs_H = -0.1857,
                         Hamiltonian_HubbardDerivs_N = -0.1535,
                         Hamiltonian_HubbardDerivs_O = -0.1575,
                         Hamiltonian_HubbardDerivs_S = -0.0695,
                         Hamiltonian_HubbardDerivs_P = -0.0702,
                         Hamiltonian_MaxAngularMomentum_='',
                         Hamiltonian_MaxAngularMomentum_O='"p"',
                         Hamiltonian_MaxAngularMomentum_H='"s"',
                         Hamiltonian_MaxAngularMomentum_C='"p"',
                         Hamiltonian_MaxAngularMomentum_N='"p"',
                         Hamiltonian_MaxAngularMomentum_P='"d"',
                         Hamiltonian_MaxAngularMomentum_S='"d"' 
                        )


# In[5]:


# DFTB3-D3 (dx.doi.org/10.1021/jz500755u) Hubbard values from dftb.org
# (note difference for P and S from https://pubs.acs.org/doi/pdf/10.1021/ct401002w)
DFTB3D3 = Dftb(label=seedname,
                         atoms=atoms,
                         kpts=None,
                         Hamiltonian_SCC = "Yes",
                         Hamiltonian_SCCTolerance = 1.00E-10,
                         Hamiltonian_Dispersion_ = "DftD3",
                         Hamiltonian_Dispersion_Damping_ = 'BeckeJohnson',
                         Hamiltonian_Dispersion_Damping_a1 = 0.5719,
                         Hamiltonian_Dispersion_Damping_a2 = 3.6017,
                         Hamiltonian_Dispersion_s6 = 1.0,
                         Hamiltonian_Dispersion_s8 = 0.5883,
                         Hamiltonian_Dispersion_Threebody = "No",
                         Hamiltonian_Dispersion_HHRepulsion = "No",
                         Hamiltonian_HCorrection_ = 'Damping',
                         Hamiltonian_HCorrection_Exponent = 4.2,  
                         Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                         Hamiltonian_ThirdOrderFull = 'Yes',          
                         Hamiltonian_HubbardDerivs_= '',
                         Hamiltonian_HubbardDerivs_C = -0.1492,
                         Hamiltonian_HubbardDerivs_H = -0.1857,
                         Hamiltonian_HubbardDerivs_N = -0.1535,
                         Hamiltonian_HubbardDerivs_O = -0.1575,
                         Hamiltonian_HubbardDerivs_S = -0.0695,
                         Hamiltonian_HubbardDerivs_P = -0.0702,
                         Hamiltonian_MaxAngularMomentum_='',
                         Hamiltonian_MaxAngularMomentum_O='"p"',
                         Hamiltonian_MaxAngularMomentum_H='"s"',
                         Hamiltonian_MaxAngularMomentum_C='"p"',
                         Hamiltonian_MaxAngularMomentum_N='"p"',
                         Hamiltonian_MaxAngularMomentum_P='"d"',
                         Hamiltonian_MaxAngularMomentum_S='"d"' 
                        )


# In[4]:


# Command to get the total energy (of the current configuration - atoms is updated/overwritten during optimisation)
atoms.set_calculator(MBD) 
ucf = ExpCellFilter(atoms)
dyn = QuasiNewton(ucf)
traj = Trajectory('cell_opt_traj.traj', 'w', atoms)
dyn.attach(traj)
dyn.run(fmax=0.001)


# In[10]:


# Check the volume and energy of the current geometry
print(atoms.get_volume())
atoms.get_total_energy()


# In[12]:


# Write optimised file - important!!!
write('BENZEN20_DFTB_MBD.cif', atoms)


# In[ ]:




