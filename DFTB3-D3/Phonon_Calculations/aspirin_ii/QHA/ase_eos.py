#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Inmport various parts of ASE - not all are used (at this stage)

import numpy as np

import ase
from ase import Atoms
from ase.calculators.dftb import Dftb
from ase.optimize import  QuasiNewton, BFGS, FIRE
from ase.constraints import ExpCellFilter
from ase.optimize.precon import Exp, PreconLBFGS, PreconFIRE
from ase.io import write, read, Trajectory
from ase.calculators.calculator import kptdensity2monkhorstpack


# In[2]:


atoms = Atoms(read('opt_ACSALA13_DFTB3.cif'), pbc=1, calculator=Dftb())

# Determine k-point grid
kpts = kptdensity2monkhorstpack(atoms,kptdensity=3.0,even=False)
offset = []
kptstr = "SupercellFolding {"+str(kpts[0])+" 0 0 0 "+str(kpts[1])+" 0 0 0 "+str(kpts[2])
for kpt in kpts:
    if kpt %2 == 0:
        offset.append(0.5)
    else:
        offset.append(0.0)
offsetstr = " "+str(offset[0])+" "+str(offset[1])+" "+str(offset[2])+" }"
print(kpts)


# In[3]:


seedname = 'EOS'

DFTD3 = Dftb(label=seedname,
                         atoms=atoms,
                         kpts=None,
                         Hamiltonian_SCC = "Yes",
                         Hamiltonian_DampXH = "Yes",
                         Hamiltonian_SCCTolerance = 1.00E-10,
                         Hamiltonian_DampXHExponent = "4.0",
                         Hamiltonian_Dispersion_ = "DftD3",
                         Hamiltonian_Dispersion_Damping_ = 'BeckeJohnson',
                         Hamiltonian_Dispersion_Damping_a1 = 0.5719,
                         Hamiltonian_Dispersion_Damping_a2 = 3.6017,
                         Hamiltonian_Dispersion_s6 = 1.0,
                         Hamiltonian_Dispersion_s8 = 0.5883,
                         Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                         Hamiltonian_ThirdOrderFull = 'Yes',
                         Hamiltonian_HubbardDerivs_= '',
                         Hamiltonian_HubbardDerivs_C = -0.1492,
                         Hamiltonian_HubbardDerivs_H = -0.1857,
                         Hamiltonian_HubbardDerivs_N = -0.1535,
                         Hamiltonian_HubbardDerivs_O = -0.1575,
                         Hamiltonian_HubbardDerivs_S = -0.1100,
                         Hamiltonian_MaxAngularMomentum_='',
                         Hamiltonian_MaxAngularMomentum_O='"p"',
                         Hamiltonian_MaxAngularMomentum_H='"s"',
                         Hamiltonian_MaxAngularMomentum_C='"p"',
                         Hamiltonian_MaxAngularMomentum_N='"p"',
                         Hamiltonian_MaxAngularMomentum_S='"d"' 
                        )


# In[7]:


cell = atoms.get_cell()
traj = Trajectory('atoms.traj', 'w')

for x in np.linspace(0.95, 1.05, 5):
    atoms.set_cell(cell * x, scale_atoms=True)
    atoms.get_potential_energy()
    traj.write(atoms)
    #print(len(int(x))


# In[17]:


from ase.io import read
from ase.units import kJ
from ase.eos import EquationOfState
configs = read('atoms.traj@0:5')  # read 10 configurations

#print(read('atoms.traj1@0'))
traj1 = Trajectory('opt_UREA_traj.traj', 'w')
# Set up optimisation of the unit cell
for i in range(5):
    print(i)
    a = read('atoms.traj@'+str(i))
    print(a)
    a.set_calculator(DFTD3) 
    ucf = ExpCellFilter(a, constant_volume=True)
    dyn = QuasiNewton(ucf)
    traj = Trajectory('cell_opt_traj.traj', 'w', a)
    print(a)
    dyn.attach(traj)
    dyn.run(fmax=0.00001)
    traj1.write(a)
    print(a)
    write('opt_UREA_DFTBD3_' + str(i) + '.gen', a)
    write('opt_UREA_DFTBD3_' + str(i) + '.cif', a)


# In[16]:


configs = read('opt_UREA_traj.traj@0:5')  # read 10 configurations

# Extract volumes and energies:
volumes = [a.get_volume()/ase.units.Ang for a in configs]
energies = [a.get_potential_energy()/ase.units.eV for a in configs]
eos = EquationOfState(volumes, energies, eos = 'birchmurnaghan')
v0, e0, B = eos.fit()
print(v0, e0, B)
eos.plot('ureaxx12-eos.png')


# In[15]:


# Write optimised file - important!!!
#write('opt_ACSALA05_DFTBD3.cif', atoms


print(volumes)
print(energies)


# In[ ]:




