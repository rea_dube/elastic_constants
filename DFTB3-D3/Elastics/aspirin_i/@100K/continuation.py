import sys
import ase
import os.path
from ase.calculators.dftb import Dftb
from ase.optimize import QuasiNewton, BFGS, FIRE
from ase.io import write, read
from ase.calculators.calculator import kptdensity2monkhorstpack
import ase.spacegroup
import scipy as sci
import re
import numpy as np
import pylab as P
import CijUtil

cif_file = 'cell_calc.traj@-1'
crystal = read(cif_file)
seedname = 'ACSALA05_DFTB3'
units = 'GPa'

write('opt_ACSALA05_DFTBD3_100K_751.cif', crystal)
write('opt_ACSALA05_DFTBD3_100K_751.gen', crystal)



