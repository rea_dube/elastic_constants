#!/usr/bin/env python
# coding: utf-8

# In[34]:


# Inmport various parts of ASE - not all are used (at this stage)

import numpy as np

import ase
from ase import Atoms
from ase.calculators.dftb import Dftb
from ase.optimize import  QuasiNewton, BFGS, FIRE
from ase.constraints import ExpCellFilter
from ase.optimize.precon import Exp, PreconLBFGS, PreconFIRE
from ase.io import write, read, Trajectory
from ase.calculators.calculator import kptdensity2monkhorstpack


# In[35]:


atoms = Atoms(read('opt_ACSALA13_DFTB3.cif'), pbc=1, calculator=Dftb())

# Determine k-point grid
kpts = kptdensity2monkhorstpack(atoms,kptdensity=3.0,even=False)
offset = []
kptstr = "SupercellFolding {"+str(kpts[0])+" 0 0 0 "+str(kpts[1])+" 0 0 0 "+str(kpts[2])
for kpt in kpts:
    if kpt %2 == 0:
        offset.append(0.5)
    else:
        offset.append(0.0)
offsetstr = " "+str(offset[0])+" "+str(offset[1])+" "+str(offset[2])+" }"
print(kpts)


# In[36]:


seedname = 'EOS'

DFTD3 = Dftb(label=seedname,
                         atoms=atoms,
                         kpts=None,
                         Hamiltonian_SCC = "Yes",
                         Hamiltonian_DampXH = "Yes",
                         Hamiltonian_SCCTolerance = 1.00E-10,
                         Hamiltonian_DampXHExponent = "4.0",
                         Hamiltonian_Dispersion = "DftD3{}",
                         Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                         Hamiltonian_ThirdOrderFull = 'Yes',
                         Hamiltonian_HubbardDerivs_= '',
                         Hamiltonian_HubbardDerivs_C = -0.1492,
                         Hamiltonian_HubbardDerivs_H = -0.1857,
                         Hamiltonian_HubbardDerivs_N = -0.1535,
                         Hamiltonian_HubbardDerivs_O = -0.1575,
                         Hamiltonian_HubbardDerivs_S = -0.1100,
                         Hamiltonian_MaxAngularMomentum_='',
                         Hamiltonian_MaxAngularMomentum_O='"p"',
                         Hamiltonian_MaxAngularMomentum_H='"s"',
                         Hamiltonian_MaxAngularMomentum_C='"p"',
                         Hamiltonian_MaxAngularMomentum_N='"p"',
                         Hamiltonian_MaxAngularMomentum_S='"d"' 
                        )


# In[38]:


cell = atoms.get_cell()
traj = Trajectory('atoms.traj', 'w')

#for x in np.linspace(0.95, 1.05, 10):
atoms.set_cell(cell * 1.03,scale_atoms=True)
atoms.get_potential_energy()
traj.write(atoms)
print(atoms)


# In[39]:


from ase.io import read
from ase.units import kJ
from ase.eos import EquationOfState
#configs = read('atoms.traj@0:10')  # read 10 configurations

#print(read('atoms.traj1@0'))
#traj1 = Trajectory('opt_UREA_traj.traj', 'w')
# Set up optimisation of the unit cell
#for i in range(10):
#print(i)
#a = read('atoms.traj@'+str(i))
#print(a)
atoms.set_calculator(DFTD3) 
traj = Trajectory('cell_calc.traj', 'w', atoms)
ucf = ExpCellFilter(atoms, constant_volume=True)
dyn = QuasiNewton(ucf)
#traj = Trajectory('cell_opt_traj.traj', 'w', a)
#print(a)
dyn.attach(traj)
dyn.run(fmax=0.00001)
#traj.write(atoms)
#print(atoms)
#write('opt_UREA_DFTBD3_' + str(i) + '.gen', atoms)


# In[40]:


#configs = read('opt_UREA_traj.traj@0:10')  # read 10 configurations

# Extract volumes and energies:
atoms.get_volume()/ase.units.Ang
#energies = [a.get_potential_energy()/ase.units.eV for a in configs]
#eos = EquationOfState(volumes, energies, eos = 'birchmurnaghan')
#v0, e0, B = eos.fit()
#print(v0, e0, B)
#eos.plot('ureaxx12-eos.png')


# In[41]:


# Write optimised file - important!!!
write('opt_ACSALA13_100.cif', atoms)


#print(volumes, energies)


# In[ ]:




