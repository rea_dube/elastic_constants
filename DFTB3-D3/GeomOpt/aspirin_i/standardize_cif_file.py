import ccdc
import os
import subprocess
import sys

def standardise_with_platon(crystal):
    identifier = crystal.identifier
    platon_exe = '/home/areilly/bin/platon/platon'
    if not os.path.isfile(platon_exe):
        print ('Error - could not find platon executable')
        sys.exit(1)

    with ccdc.io.CrystalWriter('temp.res') as res_writer:
        res_writer.write(crystal)

    with open('temp.res', 'r') as f:
        lines = f.readlines()
    with open('temp.res', 'w') as f:
        for line in lines[:-1]:
            f.write(line)
        f.write('CALC ADDSYM EXACT SHELX\n')

    devnull = open(os.devnull, 'wb')
    subprocess.call([platon_exe, 'temp.res'], stdout=devnull)

    if os.path.isfile('temp_pl.res'):
        # Sort out identifier for v. 1.0 of API
        with open('temp_pl.res', 'r') as f:
            lines = f.readlines()
        with open('temp.res', 'w') as f:
            f.write('TITL ' + identifier + '\n')
            for line in lines[1:]:
                f.write(line)
        # Return crystal object in reduced form
        reader = ccdc.io.CrystalReader('temp.res')
        crystal = reader[0]
        reader.close()

        os.remove('temp.res')
        os.remove('temp.lis')
        os.remove('temp_pl.res')
        os.remove('temp_pl.spf')
        os.remove('check.def')

        return crystal
    else:
        return None

crystal_reader = ccdc.io.CrystalReader(raw_input("Enter name of cif file(with extension):"))
crystal = crystal_reader[0]
print (round(crystal.cell_volume, 3))
new_standard_crystal = standardise_with_platon(crystal)
with ccdc.io.CrystalWriter(raw_input("Enter name of standardised file(with extension):")) as crystal_writer:
    crystal_writer.write(new_standard_crystal)
