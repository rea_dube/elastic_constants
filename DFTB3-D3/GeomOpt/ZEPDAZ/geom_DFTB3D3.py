#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Inmport various parts of ASE - not all are used (at this stage)

import ase
from ase.calculators.dftb import Dftb
from ase.optimize import  QuasiNewton, BFGS, FIRE
from ase.constraints import ExpCellFilter
from ase.optimize.precon import Exp, PreconLBFGS, PreconFIRE
from ase.io import write, read, Trajectory
from ase.calculators.calculator import kptdensity2monkhorstpack


# In[2]:


seedname = 'ZEPDAZ_DFTB3D3'
# Import your crystal structure - change name to a file in the current folder (alongside the script)
atoms = read('ZEPDAZ.cif')


# In[3]:


# Set up DFTB calculation parameters (use for crystals)
kpts = kptdensity2monkhorstpack(atoms,kptdensity=3.0,even=False)
offset = []
kptstr = "SupercellFolding {"+str(kpts[0])+" 0 0 0 "+str(kpts[1])+" 0 0 0 "+str(kpts[2])
for kpt in kpts:
    if kpt %2 == 0:
        offset.append(0.5)
    else:
        offset.append(0.0)
offsetstr = " "+str(offset[0])+" "+str(offset[1])+" "+str(offset[2])+" }"
print(kpts)
atoms.set_calculator(Dftb(label=seedname,
                         atoms=atoms,
                         kpts=None,
                         Hamiltonian_SCC = "Yes",
                         Hamiltonian_DampXH = "Yes",
                         Hamiltonian_SCCTolerance = 1.00E-10,
                         Hamiltonian_DampXHExponent = "4.0",
                         Hamiltonian_Dispersion = "DftD3{}",
                         Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                         Hamiltonian_ThirdOrderFull = 'Yes',
                         Hamiltonian_HubbardDerivs_= '',
                         Hamiltonian_HubbardDerivs_C = -0.1492,
                         Hamiltonian_HubbardDerivs_H = -0.1857,
                         Hamiltonian_HubbardDerivs_N = -0.1535,
                         Hamiltonian_HubbardDerivs_O = -0.1575,
                         Hamiltonian_HubbardDerivs_S = -0.1100,
                         Hamiltonian_HubbardDerivs_Cl = -0.0697,
                         Hamiltonian_MaxAngularMomentum_='',
                         Hamiltonian_MaxAngularMomentum_O='"p"',
                         Hamiltonian_MaxAngularMomentum_H='"s"',
                         Hamiltonian_MaxAngularMomentum_C='"p"',
                         Hamiltonian_MaxAngularMomentum_N='"p"',
                         Hamiltonian_MaxAngularMomentum_S='"d"',
                         Hamiltonian_MaxAngularMomentum_Cl='"d"'
                        )) 


# In[4]:


# Set up optimisation of the unit cell
ucf = ExpCellFilter(atoms)
dyn = QuasiNewton(ucf)
traj = Trajectory(seedname+'.traj', 'w', atoms)
dyn.attach(traj)


# In[5]:


# Run optimisation with convergence criteria of 0.005 eV/Ang for the forces
dyn.run(fmax=0.0001)


# In[6]:


# Command to get the total energy (of the current configuration - atoms is updated/overwritten during optimisation)
atoms.get_total_energy()/ase.units.eV
# Check the stress on the cell and its size
print(atoms.get_stress()/ase.units.GPa)      
print(atoms.cell)


# In[7]:


# Write optimised file - important!!!
write('opt_'+seedname+'.cif', atoms)


# In[ ]:





# In[ ]:




