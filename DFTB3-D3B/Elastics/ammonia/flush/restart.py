import sys
import os.path
import numpy as np
import re
import pylab as P

seedname = "ammonia_DFTB3D3B"
cijdat = open(seedname + ".cijdat", "r")

pattern = "cij__"
lines = cijdat.readlines()
lines.reverse()

for line in lines:
    if pattern in line:
        print(line)
        x = re.search("(\d+)__(\d+)", line)
        #Print(x.group(1), x.group(2))
        patt = int(x.group(1)) - 1
        neg = 0
        a = (int(x.group(2)) - neg - 1)/2
        if isinstance(a,int) == False:
            neg = 0
            a = (int(x.group(2)) - neg - 1)/2
        else:
            neg = 1
            a = (int(x.group(2)) - neg - 1)/2
        print("patt_arg:" ,patt)
        print("a_arg:",a)
        print("neg_arg:",neg)
        break        
    

