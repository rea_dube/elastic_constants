import sys
import ase
import os.path
from ase.calculators.dftb import Dftb
from ase.optimize import QuasiNewton, BFGS, FIRE
from ase.io import write, read
from ase.calculators.calculator import kptdensity2monkhorstpack
import ase.spacegroup
import scipy as sci
import re
import numpy as np
import pylab as P
import CijUtil


def analyse_patterns(strain):
    # these are the IRE conventions, except that integers are 0->5 rather than 1->6
    strain_dict = {0: "xx", 1: "yy", 2: "zz", 3: "yz", 4: "zx", 5: "xy"}

    strains_used = sci.zeros((6, 1))

    for a in range(0, sci.size(strain)):
        if strain[a] != 0.0:
            print((strain_dict[a], "component is non-zero"))
            strains_used[a] = 1
        else:
            strains_used[a] = 0

    return strains_used


def cmatrix(symmetry_type, tetr_high):
    if symmetry_type == "Cubic":
        return sci.matrix([[1, 7, 7, 0, 0, 0],
                           [7, 1, 7, 0, 0, 0],
                           [7, 7, 1, 0, 0, 0],
                           [0, 0, 0, 4, 0, 0],
                           [0, 0, 0, 0, 4, 0],
                           [0, 0, 0, 0, 0, 4]])

    elif symmetry_type == "Trigonal-high/Hexagonal":
        return sci.matrix([[1, 7, 8, 9, 0, 0],
                           [7, 1, 8, -9, 0, 0],
                           [8, 8, 3, 0, 0, 0],
                           [9, -9, 0, 4, 0, 0],
                           [0, 0, 0, 0, 4, 9],
                           [0, 0, 0, 0, 9, 6]])

    elif symmetry_type == "Trigonal-low":
        return sci.matrix([[1, 7, 8, 9, 10, 0],
                           [7, 1, 8, -9, -10, 0],
                           [8, 8, 3, 0, 0, 0],
                           [9, -9, 0, 4, 0, -10],
                           [10, -10, 0, 0, 4, 9],
                           [0, 0, 0, -10, 9, 6]])

    elif symmetry_type == "Tetragonal":
        if tetr_high == "-1":
            print("Higher-symmetry tetragonal (422,4mm,4-2m,4/mmm)")
            return sci.matrix([[1, 7, 8, 0, 0, 0],
                               [7, 1, 8, 0, 0, 0],
                               [8, 8, 3, 0, 0, 0],
                               [0, 0, 0, 4, 0, 0],
                               [0, 0, 0, 0, 4, 0],
                               [0, 0, 0, 0, 0, 6]])
        else:
            print("Lower-symmetry tetragonal (4,-4,4/m)")
            return sci.matrix([[1, 7, 8, 0, 0, 11],
                               [7, 1, 8, 0, 0, -11],
                               [8, 8, 3, 0, 0, 0],
                               [0, 0, 0, 4, 0, 0],
                               [0, 0, 0, 0, 4, 0],
                               [11, -11, 0, 0, 0, 6]])

    elif symmetry_type == "Orthorhombic":
        return sci.matrix([[1, 7, 8, 0, 0, 0],
                           [7, 2, 12, 0, 0, 0],
                           [8, 12, 3, 0, 0, 0],
                           [0, 0, 0, 4, 0, 0],
                           [0, 0, 0, 0, 5, 0],
                           [0, 0, 0, 0, 0, 6]])

    elif symmetry_type == "Monoclinic":
        return sci.matrix([[1, 7, 8, 0, 10, 0],
                           [7, 2, 12, 0, 14, 0],
                           [8, 12, 3, 0, 17, 0],
                           [0, 0, 0, 4, 0, 20],
                           [10, 14, 17, 0, 5, 0],
                           [0, 0, 0, 20, 0, 6]])

    elif symmetry_type == "Triclinic":
        return sci.matrix([[1, 7, 8, 9, 10, 11],
                           [7, 2, 12, 13, 14, 15],
                           [8, 12, 3, 16, 17, 18],
                           [9, 13, 16, 4, 19, 20],
                           [10, 14, 17, 19, 5, 21],
                           [11, 15, 18, 20, 21, 6]])


def spacegroup_2_strain_pat(spacegroup):
    """
    Converts point group number (as ordered by CASTEP
    and in the International Tables) to a number representing
    the needed strain pattern.
    """
    supcode = 0
    if spacegroup < 1:
        print(("Space-group number " + str(spacegroup) + " not recognized.\n"))
        sys.exit(1)
    elif spacegroup <= 2:
        # Triclinic
        patt = 1
    elif spacegroup <= 15:
        # Monoclinic
        patt = 2
    elif spacegroup <= 74:
        # Orthorhombic
        patt = 3
    elif spacegroup <= 142:
        # Tetragonal
        patt = 4
        # elif (spacegroup <= 167):
        # Trigonal-Low
        # patt = 6
    elif spacegroup <= 167:
        # Trigonal-High
        # patt = 7
        # supcode = 1
        print("Trigonal Space groups currently not supported")
        sys.exit(1)

    elif spacegroup <= 194:
        # Hexagonal
        patt = 7
    elif spacegroup <= 320:
        # Cubic
        patt = 5
    else:
        print(("Space-group number " + str(spacegroup) + " not recognized.\n"))
        sys.exit(1)
    return patt, supcode


def get_strain_patterns(code, supcode):
    """
    Given a code number for the crystal symmetry,
    returns a list of strain patterns needed for
    the calculation of the elastic constants tensor.
    Each pattern is a 6 element list, the subscript
    reflects the strain in IRE notation

    Supported Strain Patterns
    -------------------------

    5 Cubic: e1+e4
    7 Hexagonal: e3 and e1+e4
    7 Trigonal-High (32, 3m, -3m): e1 and e3+e4
    6 Trigonal-Low (3, -3): e1 and e3+e4
    4 Tetragonal: e1+e4 and e3+e6
    3 Orthorhombic: e1+e4 and e2+e5 and e3+e6
    2 Monoclinic: e1+e4 and e3+e6 and e2 and e5
    1 Triclinic: e1 to e6 separately
    0 Unknown...
    """

    if code == 1:
        pattern = [[1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                   [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
                   [0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
                   [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                   [0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                   [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]]
    elif code == 2:
        pattern = [[1.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                   [0.0, 0.0, 1.0, 0.0, 0.0, 1.0],
                   [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
                   [0.0, 0.0, 0.0, 0.0, 1.0, 0.0]]
    elif code == 3:
        pattern = [[1.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                   [0.0, 1.0, 0.0, 0.0, 1.0, 0.0],
                   [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]]
    elif code == 4:
        pattern = [[1.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                   [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]]
    elif code == 5:
        pattern = [[1.0, 0.0, 0.0, 1.0, 0.0, 0.0]]
    elif code == 6:
        pattern = [[1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                   [0.0, 0.0, 1.0, 1.0, 0.0, 0.0]]
    elif code == 7:
        # NB is this correct for hex and hig trig? - see missmatch above/
        # I suspect I have to rotate lattice for trig high?
        pattern = [[0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
                   [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]]
        if supcode == 1:
            pattern = [[1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                       [0.0, 0.0, 1.0, 1.0, 0.0, 0.0]]
    else:
        print("Error recognising pattern")
        sys.exit(1)

    return pattern


def cellabc_2_cellcart(a, b, c, alp, bet, gam):
    """
    Given three lattice vector lengths and angles, returns
    three vectors (as list of lists:
    [[a_x, a_y, a_z], [b_x, b_y, b_z], [c_x, c_y, c_z]]) representing
    the vectors on a cartesian frame.
    """
    # Get lattice vectors on cart frame from a, b, c and angles
    # For monoclinic, b // Y and c // Z
    if alp == 90.0:
        sina = 1.0
        cosa = 0.0
    else:
        sina = np.sin(np.radians(alp))
        cosa = np.cos(np.radians(alp))
    if bet == 90.0:
        sinb = 1.0
        cosb = 0.0
    else:
        sinb = np.sin(np.radians(bet))
        cosb = np.cos(np.radians(bet))
    if gam == 90.0:
        sing = 1.0
        cosg = 0.0
    else:
        sing = np.sin(np.radians(gam))
        cosg = np.cos(np.radians(gam))
    c_x = 0.0
    c_y = 0.0
    c_z = c

    b_x = 0.0
    b_y = b * sina
    b_z = b * cosa

    a_z = a * cosb
    a_y = a * (cosg - cosa * cosb) / sina
    trm1 = a_y / a
    a_x = a * np.sqrt(1.0 - cosb ** 2 - trm1 ** 2)

    return [[a_x, a_y, a_z], [b_x, b_y, b_z], [c_x, c_y, c_z]]


def cellCART2cellABC(lattice):
    """
    Given three latice vectors (with three componets each) return
    the lattice vector lengths and angles between them. Input argument
    should be [[a_x, a_y, a_z], [b_x, b_y, bz], [c_x, c_y, c_z]]. Angles
    returned in degrees.
    """
    # Does not care about orentation...
    a = np.sqrt(lattice[0][0] ** 2 + lattice[0][1] ** 2 + lattice[0][2] ** 2)
    b = np.sqrt(lattice[1][0] ** 2 + lattice[1][1] ** 2 + lattice[1][2] ** 2)
    c = np.sqrt(lattice[2][0] ** 2 + lattice[2][1] ** 2 + lattice[2][2] ** 2)
    gam = np.arccos(np.dot(lattice[0], lattice[1]) / (a * b))
    bet = np.arccos(np.dot(lattice[0], lattice[2]) / (a * c))
    alp = np.arccos(np.dot(lattice[1], lattice[2]) / (b * c))
    return a, b, c, np.degrees(alp), np.degrees(bet), np.degrees(gam)

def graphics():
        # if using graphics, do some initial set-up
    fig = P.figure(num=1, figsize=(9.5,8),facecolor='white')
    fig.subplots_adjust(left=0.07,right=0.97,top=0.97,bottom=0.07,wspace=0.5,hspace=0.5)

    for index1 in range(6):
        for index2 in range(6):
                    # position this plot in a 6x6 grid
                    sp = P.subplot(6,6,6*(index1)+index2+1)
                    sp.set_axis_off()
                    # change the labels on the axes
                    # xlabels = sp.get_xticklabels()
                    # P.setp(xlabels,'rotation',90,fontsize=7)
                    # ylabels = sp.get_yticklabels()
                    # P.setp(ylabels,fontsize=7)
                    P.text(0.4,0.4, "n/a")
    
#    P.savefig(os.path.basename(seedname)+'_fits')


def main():
    graphics()
    cif_file = 'standard_UREAXX12_DFTB3D3B_300_143.cif' #input("Enter cif file here:")
    crystal = read(cif_file)
    seedname = 'UREAXX12_DFTB3D3B' #input("Enter seedname here:")
    units = 'GPa'
    cell = crystal.get_cell()
    sg = ase.spacegroup.get_spacegroup(crystal)
    spacegroup = sg.no

    # Re-align lattice vectors on Cartesian system
    a, b, c, al, be, ga = cellCART2cellABC(cell)
    cell = cellabc_2_cellcart(a, b, c, al, be, ga)
    crystal.set_cell(cell)
    write(seedname + 'transformed.cif', crystal)

    # Not sure why the lattice types are enumerated like this, but this is how .cijdat does it...
    lattice_types = {0: "Unknown", 1: "Triclinic", 2: "Monoclinic", 3: "Orthorhombic",
                    4: "Tetragonal", 5: "Cubic", 6: "Trigonal-low", 7: "Trigonal-high/Hexagonal"}

    maxstrain = 0.1
    numsteps = 4

    lattice_code, supcode = spacegroup_2_strain_pat(spacegroup)
    print(lattice_code)

    print(("Cell parameters: a = %f gamma = %f" % (a, al)))
    print(("                 b = %f beta  = %f" % (b, be)))
    print(("                 c = %f gamma = %f \n" % (c, ga)))
    print(("Lattce vectors:  %7f %7f %7f " % (cell[0][0], cell[0][1], cell[0][2])))
    print(("                 %7f %7f %7f " % (cell[1][0], cell[1][1], cell[1][2])))
    print(("                 %7f %7f %7f \n " % (cell[2][0], cell[2][1], cell[2][2])))
    patterns = get_strain_patterns(lattice_code, supcode)
    num_strain_patterns = len(patterns)
    print(("Lattice type is ", lattice_types[lattice_code]))
    print(("Number of patterns: " + str(num_strain_patterns) + "\n"))

    if not os.path.exists(seedname + ".cijdat"):
        cijdat = open(seedname + ".cijdat", "w")
        print(("Writing strain data to ", seedname + ".cijdat"))
        cijdat.write(str(lattice_code) + ' ' + str(numsteps * 2) + ' 0 ' + '0 \n')
        cijdat.write(str(maxstrain) + "\n")

        # The order of these three loops matters for the analysis code.
        for patt in range(num_strain_patterns):
            this_pat = patterns[patt]
            for a in range(0, numsteps):
                for neg in range(0, 2):
                    if neg == 0:
                        this_mag = (float(a) + 1) / (float(numsteps)) * maxstrain
                    else:
                        this_mag = -1.0 * (float(a) + 1) / (float(numsteps)) * maxstrain
                    disps = [x * this_mag for x in patterns[patt]]
                    # Build the strain tensor (IRE convention but 1 -> 0 etc.)
                    this_strain = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
                    # diagonal elements - strain is displacment / lattice vector length
                    this_strain[0] = disps[0] / np.sqrt(cell[0][0] ** 2 + cell[0][1] ** 2 + cell[0][2] ** 2)
                    this_strain[1] = disps[1] / np.sqrt(cell[1][0] ** 2 + cell[1][1] ** 2 + cell[1][2] ** 2)
                    this_strain[2] = disps[2] / np.sqrt(cell[2][0] ** 2 + cell[2][1] ** 2 + cell[2][2] ** 2)
                    # off diagonals - we only strain upper right corner of cell matrix, so strain is 1/2*du/dx...
                    this_strain[3] = 0.5 * (disps[3] / np.sqrt(cell[1][0] ** 2 + cell[1][1] ** 2 + cell[1][2] ** 2))
                    this_strain[4] = 0.5 * (disps[4] / np.sqrt(cell[0][0] ** 2 + cell[0][1] ** 2 + cell[0][2] ** 2))
                    this_strain[5] = 0.5 * (disps[5] / np.sqrt(cell[0][0] ** 2 + cell[0][1] ** 2 + cell[0][2] ** 2))

                    # Deform cell - only apply deformation to upper right corner
                    defcell = [[cell[0][0] + disps[0], cell[0][1] + disps[5], cell[0][2] + disps[4]],
                               [cell[1][0], cell[1][1] + disps[1], cell[1][2] + disps[3]],
                               [cell[2][0], cell[2][1], cell[2][2] + disps[2]]]

                    pattern_name = seedname + "_cij__" + str(patt + 1) + "__" + str((a * 2) + 1 + neg)

                    print(("Pattern Name = ", pattern_name))
                    print(("Pattern = ", this_pat))
                    print(("Magnitude = ", this_mag))
                    cijdat.write(pattern_name + "\n")
                    cijdat.write(str(this_strain[0]) + " " + str(this_strain[5]) + " " + str(this_strain[4]) + "\n")
                    cijdat.write(str(this_strain[5]) + " " + str(this_strain[1]) + " " + str(this_strain[3]) + "\n")
                    cijdat.write(str(this_strain[4]) + " " + str(this_strain[3]) + " " + str(this_strain[2]) + "\n")
                    # print defcell
                    # print crystal.get_cell()
                    crystal = read(cif_file)
                    crystal.set_cell(defcell, scale_atoms=True)
                    # print crystal.get_cell()

                    kpts = kptdensity2monkhorstpack(crystal, kptdensity=3.0, even=False)
                    offset = []
                    kptstr = "SupercellFolding {" + str(kpts[0]) + " 0 0 0 " + str(kpts[1]) + " 0 0 0 " + str(kpts[2])
                    for kpt in kpts:
                        if kpt % 2 == 0:
                            offset.append(0.5)
                        else:
                            offset.append(0.0)
                    offsetstr = " " + str(offset[0]) + " " + str(offset[1]) + " " + str(offset[2]) + " }"

                    crystal.set_calculator(Dftb(label='elastics',
                                                 atoms=None,
                                                 kpts=None,
                                                 Hamiltonian_SCC="Yes",
                                                 Hamiltonian_SCCTolerance = 1.00E-10,
                                                 Hamiltonian_Dispersion_ = "DftD3",
                                                 Hamiltonian_Dispersion_Damping_ = 'BeckeJohnson',
                                                 Hamiltonian_Dispersion_Damping_a1 = 1.3719,
                                                 Hamiltonian_Dispersion_Damping_a2 = 3.7017,
                                                 Hamiltonian_Dispersion_s6 = 1.0,
                                                 Hamiltonian_Dispersion_s8 = 0.5883,
                                                 Hamiltonian_Dispersion_Threebody = "No",
                                                 Hamiltonian_Dispersion_HHRepulsion = "No",
                                                 Hamiltonian_HCorrection_ = 'Damping',
                                                 Hamiltonian_HCorrection_Exponent = 4.2,  
                                                 Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                                                 Hamiltonian_ThirdOrderFull = 'Yes',          
                                                 Hamiltonian_HubbardDerivs_= '',
                                                 Hamiltonian_HubbardDerivs_C = -0.1492,
                                                 Hamiltonian_HubbardDerivs_H = -0.1857,
                                                 Hamiltonian_HubbardDerivs_N = -0.1535,
                                                 Hamiltonian_HubbardDerivs_O = -0.1575,
                                                 Hamiltonian_HubbardDerivs_S = -0.0695,
                                                 Hamiltonian_HubbardDerivs_P = -0.0702,
                                                 Hamiltonian_MaxAngularMomentum_='',
                                                 Hamiltonian_MaxAngularMomentum_O='"p"',
                                                 Hamiltonian_MaxAngularMomentum_H='"s"',
                                                 Hamiltonian_MaxAngularMomentum_C='"p"',
                                                 Hamiltonian_MaxAngularMomentum_N='"p"',
                                                 Hamiltonian_MaxAngularMomentum_P='"d"',
                                                 Hamiltonian_MaxAngularMomentum_S='"d"'                                               ))
                    dyn = QuasiNewton(crystal)
                    dyn.run(fmax=0.001, steps=200)
                    #dyn = FIRE(crystal)
                    #dyn.run(fmax=0.005)
                    stress = crystal.get_stress() / ase.units.GPa
                    cijdat.write(str(stress[0]) + ' ' + str(stress[1]) + ' ' + str(stress[2]) + ' ' + str(stress[3])
                                 + ' ' + str(stress[4]) + ' ' + str(stress[5]) + '\n')
        cijdat.close()

    # Now analysis the strains

    # initialise 1d array to store all 21 unique elastic constants - will be transformed into 6x6 matrix later
    final_cijs = sci.zeros((21, 1))
    errors = sci.zeros((21, 1))

    cijdat = open(seedname + ".cijdat", "r")
    print(("\nReading strain data from ", seedname + ".cijdat\n"))
    num_strain_patterns = (len(cijdat.readlines()) - 2) / 5
    print(num_strain_patterns)
    # rewind
    cijdat.seek(0)
    lattice_type, numsteps, tetr_high, trig_high = cijdat.readline().split()
    numsteps = int(numsteps)
    symmetry_type = lattice_types[int(lattice_type)]
    print(("System is", symmetry_type, "\n"))
    # get maximum magnitude of strains
    magnitude = float(cijdat.readline())
    print((numsteps, "steps of maximum magnitude", magnitude))

    for calc in range(0,int(num_strain_patterns/numsteps)):
        print(("\nAnalysing pattern", calc + 1, ":"))

        for a in range(0, numsteps):

            pattern = cijdat.readline()
            print(pattern)
            # grab the strain data from the .cijdat file
            line1 = cijdat.readline().split()
            line2 = cijdat.readline().split()
            line3 = cijdat.readline().split()
            line4 = cijdat.readline().split()

            # only take from the top right triangle
            # numbering according to IRE conventions (Proc IRE, 1949)

            if a == 0:
                strain = sci.array(
                    [float(line1[0]), float(line2[1]), float(line3[2]), 2 * float(line2[2]), 2 * float(line1[2]),
                     2 * float(line1[1])])
            else:
                strain = sci.row_stack((strain, sci.array(
                    [float(line1[0]), float(line2[1]), float(line3[2]), 2 * float(line2[2]), 2 * float(line1[2]),
                     2 * float(line1[1])])))

                # now get corresponding stress data from .castep
            thisStress = sci.array([float(line4[0]), float(line4[1]), float(line4[2]), float(line4[3]),
                                    float(line4[4]), float(line4[5])])
            print(a)
            # again, top right triangle
            if a == 0:
                stress = thisStress
            else:
                stress = sci.row_stack((stress, thisStress))

        """
        Both the stress and strain matrices use the IRE conventions to reduce the
        3x3 matrices to 1x6 arrays. These 1D arrays are then stacked to form a 
        Nx6 array, where N=number of steps.

        Note that strain and stress arrays are numbered 0->5 rather than 1->6
        """

            
        colourDict = {0: '#BAD0EF', 1:'#FFCECE', 2:'#BDF4CB', 3:'#EEF093',4:'#FFA4FF',5:'#75ECFD'}
        
        def __fit(index1, index2):
            from scipy import stats, sqrt, square

            
            # do the fit
            (cijFitted, intercept, r, tt, stderr) = stats.linregress(strain[:, index2 - 1], stress[:, index1 - 1])

            (vmajor, vminor, vmicro) = re.split('\.', sci.__version__)

            if int(vmajor) > 0 or int(vminor) >= 7:
                error = stderr
            else:
                # correct for scipy weirdness - see http://www.scipy.org/scipy/scipy/ticket/8
                # This was fixed before 0.7.0 release. Maybe in some versions of 0.6.x too -
                # will report huge errors if the check is wrong
                stderr = sci.sqrt((numsteps * stderr ** 2) / (numsteps - 2))
                error = stderr / sqrt(sum(square(strain[:, index2 - 1])))

            # print info about the fit
            print('\n')
            print(('Cij (gradient)          :    ', cijFitted))
            print(('Error in Cij            :    ', error))
            print(('Intercept               :    ', intercept))
            if abs(r) > 0.9:
                print(('Correlation coefficient :    ', r))
            else:
                print(('Correlation coefficient :    ', r, '     <----- WARNING'))
                # if using graphics, add a subplot

            # position this plot in a 6x6 grid
            sp = P.subplot(6,6,6*(index1-1)+index2)
            sp.set_axis_on()

            # change the labels on the axes
            xlabels = sp.get_xticklabels()
            P.setp(xlabels,'rotation',90,fontsize=7)
            ylabels = sp.get_yticklabels()
            P.setp(ylabels,fontsize=7)

            # colour the plot depending on the strain pattern
            #sp.set_facecolor(colourDict[patt])
            sp.set_facecolor(colourDict[calc])
            print(("calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+str(calc)))
            # plot the data
            P.plot([strain[0,index2-1],strain[numsteps-1,index2-1]],[cijFitted*strain[0,index2-1]+intercept,cijFitted*strain[numsteps-1,index2-1]+intercept])
            P.plot(strain[:,index2-1],stress[:,index1-1],'ro')

            return cijFitted, error

        def __append_or_replace(valList, erList, val):
            try:
                valList.append(val[0])
                erList.append(val[1])
                return (sum(valList) / len(valList)), (sci.sqrt(sum([x ** 2 for x in erList]) / len(erList) ** 2))
            except NameError:
                return val[0], val[1]

        def __create_list_and_append(val):
            new_list = [val[0]]
            error_list = [val[1]]
            return val[0], new_list, val[1], error_list

        # Analyse the patterns to see which strains were applied
        strains_used = analyse_patterns(strain[0, :])
        # should check strains are as expected

        if symmetry_type == "Cubic":

            if sci.all(strains_used.transpose() == sci.array([[1.0, 0.0, 0.0, 1.0, 0.0, 0.0]])):  # strain pattern e1+e4

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                fit_21, fit_21_error = __fit(2, 1)
                fit_31, fit_31_error = __fit(3, 1)
                final_cijs[6] = (fit_21 + fit_31) / 2  # fit C21+C31
                errors[6] = sci.sqrt((fit_21_error ** 2) / 4 + (fit_31_error ** 2) / 4)
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44

            else:
                print("Unsupported strain pattern")
                sys.exit(1)

        elif symmetry_type == "Trigonal-high/Hexagonal":
            if sci.all(strains_used.transpose() == sci.array(
                    [[0.0, 0.0, 1.0, 0.0, 0.0, 0.0]])):  # strain pattern e3 (hexagonal)

                # fit C13 + C23, and add to list (more values coming...)
                final_cijs[7], cij13, errors[7], er13 = __create_list_and_append(__fit(1, 3))
                final_cijs[7], cij13, errors[7], er13 = __create_list_and_append(__fit(2, 3))

                final_cijs[2], errors[2] = __fit(3, 3)  # fit C33

            elif sci.all(strains_used.transpose() == sci.array(
                    [[1.0, 0.0, 0.0, 1.0, 0.0, 0.0]])):  # strain pattern e1+e4 (hexagonal)

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                final_cijs[6], errors[6] = __fit(2, 1)  # fit C21
                final_cijs[7], errors[7] = __append_or_replace(cij13, er13, __fit(3, 1))  # fit C31
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44

            elif sci.all(strains_used.transpose() == sci.array([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0]])):

                # strain pattern e1 (trigonal-high)

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                final_cijs[6], errors[6] = __fit(2, 1)  # fit C21
                final_cijs[7], errors[7] = __fit(3, 1)  # fit C31
                final_cijs[8], errors[8] = __fit(4, 1)  # fit C41
            # Should be zero? finalCijs[9], errors[9] = __fit(5,1)                # fit C51

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 1.0, 1.0, 0.0, 0.0]])):

                # strain pattern e3+e4 (trigonal-high)
                # could recalculate C13/C14/C23/C24/C46 here, but won't just now

                final_cijs[2], errors[2] = __fit(3, 3)  # fit C33
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44

            else:
                print("Unsupported strain pattern")
                sys.exit(1)

        elif symmetry_type == "Trigonal-low":
            if sci.all(strains_used.transpose() == sci.array([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0]])):

                # strain pattern e1

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                final_cijs[6], errors[6] = __fit(2, 1)  # fit C21
                final_cijs[7], errors[7] = __fit(3, 1)  # fit C31
                final_cijs[8], errors[8] = __fit(4, 1)  # fit C41
                final_cijs[9], errors[9] = __fit(5, 1)  # fit C51

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 1.0, 1.0, 0.0, 0.0]])):

                # strain pattern e3+e4
                # could recalculate C13/C14/C23/C24/C46 here, but won't just now

                final_cijs[2], errors[2] = __fit(3, 3)  # fit C33
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44

            else:
                print("Unsupported strain pattern")
                sys.exit(1)

        elif symmetry_type == "Tetragonal":
            if sci.all(strains_used.transpose() == sci.array([[1.0, 0.0, 0.0, 1.0, 0.0, 0.0]])):  # strain pattern e1+e4

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                final_cijs[6], errors[6] = __fit(2, 1)  # fit C21
                final_cijs[7], errors[7] = __fit(3, 1)  # fit C31
                final_cijs[10], errors[10] = __fit(6, 1)  # fit C61
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 1.0, 0.0, 0.0, 1.0]])):  # strain pattern e3+e6

                final_cijs[2], errors[2] = __fit(3, 3)  # fit C33
                final_cijs[5], errors[5] = __fit(6, 6)  # fit C66

            else:
                print("Unsupported strain pattern")
                sys.exit(1)

        elif symmetry_type == "Orthorhombic":
            if sci.all(strains_used.transpose() == sci.array([[1.0, 0.0, 0.0, 1.0, 0.0, 0.0]])):  # strain pattern e1+e4

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                final_cijs[6], cij12, errors[6], er12 = __create_list_and_append(__fit(2, 1))  # fit C21
                final_cijs[7], cij13, errors[7], er13 = __create_list_and_append(__fit(3, 1))  # fit C31
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 1.0, 0.0, 0.0, 1.0, 0.0]])):  # strain pattern e2+e5

                final_cijs[6], errors[6] = __append_or_replace(cij12, er12, __fit(1, 2))  # fit C12
                final_cijs[1], errors[1] = __fit(2, 2)  # fit C22
                final_cijs[11], cij23, errors[11], er23 = __create_list_and_append(__fit(3, 2))  # fit C32
                final_cijs[4], errors[4] = __fit(5, 5)  # fit C55

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 1.0, 0.0, 0.0, 1.0]])):  # strain pattern e3+e6

                final_cijs[7], errors[7] = __append_or_replace(cij13, er13, __fit(1, 3))  # fit C13
                final_cijs[11], errors[11] = __append_or_replace(cij23, er23, __fit(2, 3))  # fit C23
                final_cijs[2], errors[2] = __fit(3, 3)  # fit C33
                final_cijs[5], errors[5] = __fit(6, 6)  # fit C66

            else:
                print("Unsupported strain pattern")
                sys.exit(1)

        elif symmetry_type == "Monoclinic":
            if sci.all(strains_used.transpose() == sci.array([[1.0, 0.0, 0.0, 1.0, 0.0, 0.0]])):  # strain pattern e1+e4

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                final_cijs[6], cij12, errors[6], er12 = __create_list_and_append(__fit(2, 1))  # fit C21
                final_cijs[7], cij13, errors[7], er13 = __create_list_and_append(__fit(3, 1))  # fit C31
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44
                final_cijs[9], cij51, errors[9], er51 = __create_list_and_append(__fit(5, 1))  # fit C51
                final_cijs[19], cij64, errors[19], er64 = __create_list_and_append(__fit(6, 4))  # fit C64

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 1.0, 0.0, 0.0, 1.0]])):  # strain pattern e3+e6

                final_cijs[7], errors[7] = __append_or_replace(cij13, er13, __fit(1, 3))  # fit C13
                final_cijs[11], cij23, errors[11], er23 = __create_list_and_append(__fit(2, 3))  # fit C23
                final_cijs[2], errors[2] = __fit(3, 3)  # fit C33
                final_cijs[16], cij53, errors[16], er53 = __create_list_and_append(__fit(5, 3))  # fit C53
                final_cijs[19], errors[19] = __append_or_replace(cij64, er64, __fit(4, 6))  # fit C46
                final_cijs[5], errors[5] = __fit(6, 6)  # fit C66

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 1.0, 0.0, 0.0, 0.0, 0.0]])):  # strain pattern e2

                final_cijs[6], errors[6] = __append_or_replace(cij12, er12, __fit(1, 2))  # fit C12
                final_cijs[1], errors[1] = __fit(2, 2)  # fit C22
                final_cijs[11], errors[11] = __append_or_replace(cij23, er23, __fit(3, 2))  # fit C32
                final_cijs[13], cij52, errors[13], er52 = __create_list_and_append(__fit(5, 2))  # fit C52

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 0.0, 0.0, 1.0, 0.0]])):  # strain pattern e5

                final_cijs[9], errors[9] = __append_or_replace(cij51, er51, __fit(1, 5))  # fit C15
                final_cijs[13], errors[13] = __append_or_replace(cij52, er52, __fit(2, 5))  # fit C25
                final_cijs[16], errors[16] = __append_or_replace(cij53, er53, __fit(3, 5))  # fit C35
                final_cijs[4], errors[4] = __fit(5, 5)  # fit C55
            else:
                print("Unsupported strain pattern")
                sys.exit(1)

        elif symmetry_type == "Triclinic":

            if sci.all(strains_used.transpose() == sci.array([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0]])):  # strain pattern e1

                final_cijs[0], errors[0] = __fit(1, 1)  # fit C11
                final_cijs[6], cij12, errors[6], er12 = __create_list_and_append(__fit(2, 1))  # fit C21
                final_cijs[7], cij13, errors[7], er13 = __create_list_and_append(__fit(3, 1))  # fit C31
                final_cijs[8], cij14, errors[8], er14 = __create_list_and_append(__fit(4, 1))  # fit C41
                final_cijs[9], cij15, errors[9], er15 = __create_list_and_append(__fit(5, 1))  # fit C51
                final_cijs[10], cij16, errors[10], er16 = __create_list_and_append(__fit(6, 1))  # fit C61

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 1.0, 0.0, 0.0, 0.0, 0.0]])):  # strain pattern e2

                final_cijs[6], errors[6] = __append_or_replace(cij12, er12, __fit(1, 2))  # fit C12
                final_cijs[1], errors[1] = __fit(2, 2)  # fit C22
                final_cijs[11], cij23, errors[11], er23 = __create_list_and_append(__fit(3, 2))  # fit C32
                final_cijs[12], cij24, errors[12], er24 = __create_list_and_append(__fit(4, 2))  # fit C42
                final_cijs[13], cij25, errors[13], er25 = __create_list_and_append(__fit(5, 2))  # fit C52
                final_cijs[14], cij26, errors[14], er26 = __create_list_and_append(__fit(6, 2))  # fit C62

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 1.0, 0.0, 0.0, 0.0]])):  # strain pattern e3

                final_cijs[7], errors[7] = __append_or_replace(cij13, er13, __fit(1, 3))  # fit C13
                final_cijs[11], errors[11] = __append_or_replace(cij23, er23, __fit(2, 3))  # fit C23
                final_cijs[2], errors[2] = __fit(3, 3)  # fit C33
                final_cijs[15], cij34, errors[15], er34 = __create_list_and_append(__fit(4, 3))  # fit C43
                final_cijs[16], cij35, errors[16], er35 = __create_list_and_append(__fit(5, 3))  # fit C53
                final_cijs[17], cij36, errors[17], er36 = __create_list_and_append(__fit(6, 3))  # fit C63

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 0.0, 1.0, 0.0, 0.0]])):  # strain pattern e4

                final_cijs[8], errors[8] = __append_or_replace(cij14, er14, __fit(1, 4))  # fit C14
                final_cijs[12], errors[12] = __append_or_replace(cij24, er24, __fit(2, 4))  # fit C24
                final_cijs[15], errors[15] = __append_or_replace(cij34, er34, __fit(3, 4))  # fit C34
                final_cijs[3], errors[3] = __fit(4, 4)  # fit C44
                final_cijs[18], cij45, errors[18], er45 = __create_list_and_append(__fit(5, 4))  # fit C54
                final_cijs[19], cij46, errors[19], er46 = __create_list_and_append(__fit(6, 4))  # fit C64

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 0.0, 0.0, 1.0, 0.0]])):  # strain pattern e5

                final_cijs[9], errors[9] = __append_or_replace(cij15, er15, __fit(1, 5))  # fit C15
                final_cijs[13], errors[13] = __append_or_replace(cij25, er25, __fit(2, 5))  # fit C25
                final_cijs[16], errors[16] = __append_or_replace(cij35, er35, __fit(3, 5))  # fit C35
                final_cijs[18], errors[18] = __append_or_replace(cij45, er45, __fit(4, 5))  # fit C45
                final_cijs[4], errors[4] = __fit(5, 5)  # fit C55
                final_cijs[20], cij56, errors[20], er56 = __create_list_and_append(__fit(6, 5))  # fit C65

            elif sci.all(strains_used.transpose() == sci.array([[0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])):  # strain pattern e6

                final_cijs[10], errors[10] = __append_or_replace(cij16, er16, __fit(1, 6))  # fit C16
                final_cijs[14], errors[14] = __append_or_replace(cij26, er26, __fit(2, 6))  # fit C26
                final_cijs[17], errors[17] = __append_or_replace(cij36, er36, __fit(3, 6))  # fit C36
                final_cijs[19], errors[19] = __append_or_replace(cij46, er46, __fit(4, 6))  # fit C46
                final_cijs[20], errors[20] = __append_or_replace(cij56, er56, __fit(5, 6))  # fit C56
                final_cijs[5], errors[5] = __fit(6, 6)  # fit C66
            else:
                print("Unsupported strain pattern")
                sys.exit(1)
        else:
            print("Unsupported symmetry type. Exiting")
            sys.exit(1)
        print('finished pattern')

    cijdat.close()
    if symmetry_type == "Trigonal-high/Hexagonal" or symmetry_type == "Trigonal-low":
        # for these systems, C66 is calculated as a combination of the other Cijs.
        final_cijs[5] = 0.5 * (final_cijs[0] - final_cijs[6])
        errors[5] = sci.sqrt(0.25 * (errors[0] ** 2 + errors[6] ** 2))

    c = cmatrix(symmetry_type, tetr_high)

    # Generate the 6x6 matrix of elastic constants
    # - negative values signify a symmetry relation
    final_cij_matrix = sci.zeros((6, 6))
    final_errors = sci.zeros((6, 6))
    for i in range(0, 6):
        for j in range(0, 6):
            index = int(c[i, j])
            if index > 0:
                final_cij_matrix[i, j] = final_cijs[index - 1]
                final_errors[i, j] = errors[index - 1]
            elif index < 0:
                final_cij_matrix[i, j] = -final_cijs[-index - 1]
                final_errors[i, j] = errors[-index - 1]

    # TODO Add additional tests based on sufficient & necessary conditions
    # Tests
    if symmetry_type == "Cubic":
        if final_cijs[3] <= 0:
            print("\n *** WARNING: C44 is less than or equal to zero ***\n")
        if final_cijs[0] <= abs(final_cijs[6]):
            print("\n *** WARNING: C11 is less than or equal to |C12| ***\n")
        if (final_cijs[0] + 2 * final_cijs[6]) <= 0:
            print("\n *** WARNING: C11+2C12 is less than or equal to zero ***\n")

    print("\n<>---------------------------- RESULTS ----------------------------------<>\n")
    print(("Final Cij matrix (" + units + "):"))
    print((sci.array2string(final_cij_matrix, max_line_width=130, suppress_small=True)))
    print(("\nErrors on Cij matrix (" + units + "):"))
    print((sci.array2string(final_errors, max_line_width=130, suppress_small=True)))

    (sij, esij, covsij) = CijUtil.invertCij(final_cij_matrix, final_errors)

    print(("\nFinal Sij matrix (" + units + "-1):"))
    print((sci.array2string(sij, max_line_width=130, suppress_small=True)))
    print(("\nErrors on Sij matrix (" + units + "-1):"))
    print((sci.array2string(esij, max_line_width=130, suppress_small=True)))

    print("\n<>----------------------------------------------------------------------<>\n")
    if symmetry_type == "Cubic":
        print(("  Zener anisotropy index     : %6.5f +/- %6.5f" % (CijUtil.zenerAniso(final_cij_matrix, final_errors))))
    print(("  Universal anisotropy index : %6.5f +/- %6.5f" % (CijUtil.uAniso(final_cij_matrix, final_errors))))
    print("  (Rangnthn and Ostoja-Starzewski, PRL 101, 055504)\n")

    (youngX, youngY, youngZ, eyoungX, eyoungY, eyoungZ,
     poissonXY, poissonXZ, poissonYX, poissonYZ, poissonZX, poissonZY,
     epoissonXY, epoissonXZ, epoissonYX, epoissonYZ, epoissonZX, epoissonZY) = CijUtil.youngsmod(final_cij_matrix,
                                                                                                 final_errors)

    print("\n                          x           y           z")
    print(("%18s : %11.5f %11.5f %11.5f %6s" % ("Young's Modulus", youngX, youngY, youngZ, units)))
    print(("%18s : %11.5f %11.5f %11.5f " % ("      +/-      ", eyoungX, eyoungY, eyoungZ)))

    print("\n                        xy       xz       yx       yz       zx       zy")
    output_format = "%18s :  %6.5f  %6.5f  %6.5f  %6.5f  %6.5f  %6.5f"
    print((output_format % ("Poisson's Ratios", poissonXY, poissonXZ, poissonYX, poissonYZ, poissonZX, poissonZY)))
    print((output_format % ("             +/-", epoissonXY, epoissonXZ, epoissonYX, epoissonYZ, epoissonZX, epoissonZY)))

    print("\n<>--------------------- POLYCRYSTALLINE RESULTS -------------------------<>\n")
    (voigtB, reussB, voigtG, reussG, hillB, hillG, evB, erB, evG, erG, ehB, ehG) = CijUtil.polyCij(final_cij_matrix,
                                                                                                   final_errors)
    output_format = "%16s : %11.5f %11.5f %11.5f %11.5f %11.5f %11.5f %6s"
    print("                     Voigt         +/-       Reuss         +/-       Hill          +/-")
    print((output_format % ("Bulk Modulus", voigtB, evB, reussB, erB, hillB, ehB, units)))
    print((output_format % ("Shear Modulus", voigtG, evG, reussG, erG, hillG, ehG, units)))

    print("\n<>-----------------------------------------------------------------------<>\n")

    sci.savetxt(seedname + '_cij.txt', final_cij_matrix)
    
    P.savefig(os.path.basename(seedname)+'_fits')

def calculate(outfile, files, params=None, paramfile=None):
    return main([outfile,files], libmode=True)


if __name__ == "__main__":
    main()
