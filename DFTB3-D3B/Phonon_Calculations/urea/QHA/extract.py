#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Inmport various parts of ASE - not all are used (at this stage)

import numpy as np

import ase
from ase import Atoms
from ase.calculators.dftb import Dftb
from ase.optimize import  QuasiNewton, BFGS, FIRE
from ase.constraints import ExpCellFilter
from ase.optimize.precon import Exp, PreconLBFGS, PreconFIRE
from ase.io import write, read, Trajectory
from ase.calculators.calculator import kptdensity2monkhorstpack

from ase.io import read
from ase.units import kJ
from ase.eos import EquationOfState

atoms1 = Atoms(read('opt_UREA_DFTBD3_0.cif'))
atoms2 = Atoms(read('opt_UREA_DFTBD3_1.cif'))
atoms3 = Atoms(read('opt_UREA_DFTBD3_2.cif'))
atoms4 = Atoms(read('opt_UREA_DFTBD3_3.cif'))
atoms5 = Atoms(read('opt_UREA_DFTBD3_4.cif'))

traj1 = Trajectory('a.traj', 'w', atoms1)
traj2 = Trajectory('a.traj', 'a', atoms2)
traj3 = Trajectory('a.traj', 'a', atoms3)
traj4 = Trajectory('a.traj', 'a', atoms4)
traj5 = Trajectory('a.traj', 'a', atoms5)

traj1.write(atoms1)
traj2.write(atoms2)
traj3.write(atoms3)
traj4.write(atoms4)
traj5.write(atoms5)

atoms = Atoms(read('a.traj'))

seedname = 'EOS'

# Determine k-point grid
kpts = kptdensity2monkhorstpack(atoms,kptdensity=3.0,even=False)
offset = []
kptstr = "SupercellFolding {"+str(kpts[0])+" 0 0 0 "+str(kpts[1])+" 0 0 0 "+str(kpts[2])
for kpt in kpts:
    if kpt %2 == 0:
        offset.append(0.5)
    else:
        offset.append(0.0)
offsetstr = " "+str(offset[0])+" "+str(offset[1])+" "+str(offset[2])+" }"
print(kpts)


DFTD3 = Dftb(label=seedname,
                         atoms = atoms,
                         kpts=None,
                         Hamiltonian_SCC = "Yes",
                         Hamiltonian_DampXH = "Yes",
                         Hamiltonian_SCCTolerance = 1.00E-10,
                         Hamiltonian_DampXHExponent = "4.0",
                         Hamiltonian_Dispersion_ = "DftD3",
                         Hamiltonian_Dispersion_Damping_ = 'BeckeJohnson',
                         Hamiltonian_Dispersion_Damping_a1 = 0.5719,
                         Hamiltonian_Dispersion_Damping_a2 = 3.6017,
                         Hamiltonian_Dispersion_s6 = 1.0,
                         Hamiltonian_Dispersion_s8 = 0.5883,
                         Hamiltonian_KPointsAndWeights = kptstr+offsetstr,
                         Hamiltonian_ThirdOrderFull = 'Yes',
                         Hamiltonian_HubbardDerivs_= '',
                         Hamiltonian_HubbardDerivs_C = -0.1492,
                         Hamiltonian_HubbardDerivs_H = -0.1857,
                         Hamiltonian_HubbardDerivs_N = -0.1535,
                         Hamiltonian_HubbardDerivs_O = -0.1575,
                         Hamiltonian_HubbardDerivs_S = -0.1100,
                         Hamiltonian_MaxAngularMomentum_='',
                         Hamiltonian_MaxAngularMomentum_O='"p"',
                         Hamiltonian_MaxAngularMomentum_H='"s"',
                         Hamiltonian_MaxAngularMomentum_C='"p"',
                         Hamiltonian_MaxAngularMomentum_N='"p"',
                         Hamiltonian_MaxAngularMomentum_S='"d"'
                        )

traj = Trajectory('cell_opt.traj', 'w')

for i in range(5):
    print(i)
    a = read('atoms.traj@'+str(i))
    print(a)
    DFTD3.calculate(a, properties = ['energy','stress','forces'])
    traj.write(a)
    
 
#atoms.set_calculator(DFTD3)
#traj.write(atoms)

configs = read('cell_opt.traj@0:5')  # read 10 configurations

# Extract volumes and energies:
volumes = [a.get_volume()/ase.units.Ang for a in configs]
energies = [a.get_potential_energy()/ase.units.eV for a in configs]
eos = EquationOfState(volumes, energies, eos = 'birchmurnaghan')
v0, e0, B = eos.fit()
print(v0, e0, B)
eos.plot('ureaxx12-eos.png')


# In[15]:


# Write optimised file - important!!!
#write('opt_ACSALA05_DFTBD3.cif', atoms


print(volumes)
print(energies)


