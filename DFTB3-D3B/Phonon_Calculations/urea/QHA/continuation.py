import numpy as np

import ase
from ase import Atoms
from ase.calculators.dftb import Dftb
from ase.optimize import  QuasiNewton, BFGS, FIRE
from ase.constraints import ExpCellFilter
from ase.optimize.precon import Exp, PreconLBFGS, PreconFIRE
from ase.io import write, read, Trajectory
from ase.calculators.calculator import kptdensity2monkhorstpack

from ase.io import read
from ase.units import kJ
from ase.eos import EquationOfState


configs = read('opt_UREA_traj.traj@0:5')  # read 10 configurations

# Extract volumes and energies:
volumes = [a.get_volume()/ase.units.Ang for a in configs]
energies = [a.get_potential_energy()/ase.units.eV for a in configs]
eos = EquationOfState(volumes, energies, eos = 'birchmurnaghan')
v0, e0, B = eos.fit()
print(v0, e0, B)
eos.plot('ureaxx12-eos.png')

