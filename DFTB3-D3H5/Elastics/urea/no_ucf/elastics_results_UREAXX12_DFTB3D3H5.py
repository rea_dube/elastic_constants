4
Cell parameters: a = 5.563900 gamma = 90.000000
                 b = 5.563900 beta  = 90.000000
                 c = 4.643700 gamma = 90.000000 

Lattce vectors:  5.563900 0.000000 0.000000 
                 0.000000 5.563900 0.000000 
                 0.000000 0.000000 4.643700 
 
Lattice type is  Tetragonal
Number of patterns: 2

Writing strain data to  UREAXX12_DFTB3D3H5.cijdat
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__1
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  0.013333333333333332
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:05:50     -620.130694        0.1238
BFGSLineSearch:    1[  2] 16:05:52     -620.130921        0.0338
BFGSLineSearch:    2[  4] 16:05:53     -620.131038        0.0279
BFGSLineSearch:    3[  6] 16:05:55     -620.131077        0.0155
BFGSLineSearch:    4[  8] 16:05:57     -620.131100        0.0141
BFGSLineSearch:    5[ 10] 16:05:59     -620.131113        0.0100
BFGSLineSearch:    6[ 12] 16:06:01     -620.131129        0.0083
BFGSLineSearch:    7[ 14] 16:06:03     -620.131136        0.0053
BFGSLineSearch:    8[ 16] 16:06:05     -620.131139        0.0053
BFGSLineSearch:    9[ 17] 16:06:06     -620.131155        0.0030
BFGSLineSearch:   10[ 19] 16:06:08     -620.131157        0.0075
BFGSLineSearch:   11[ 21] 16:06:10     -620.131162        0.0025
BFGSLineSearch:   12[ 22] 16:06:11     -620.131165        0.0019
BFGSLineSearch:   13[ 23] 16:06:12     -620.131168        0.0019
BFGSLineSearch:   14[ 24] 16:06:13     -620.131169        0.0037
BFGSLineSearch:   15[ 26] 16:06:15     -620.131169        0.0022
BFGSLineSearch:   16[ 27] 16:06:15     -620.131171        0.0014
BFGSLineSearch:   17[ 28] 16:06:16     -620.131171        0.0010
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__2
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  -0.013333333333333332
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:06:18     -620.130701        0.1228
BFGSLineSearch:    1[  2] 16:06:20     -620.130921        0.0335
BFGSLineSearch:    2[  4] 16:06:22     -620.131038        0.0265
BFGSLineSearch:    3[  6] 16:06:24     -620.131074        0.0154
BFGSLineSearch:    4[  8] 16:06:26     -620.131099        0.0125
BFGSLineSearch:    5[ 10] 16:06:28     -620.131111        0.0105
BFGSLineSearch:    6[ 12] 16:06:30     -620.131127        0.0083
BFGSLineSearch:    7[ 14] 16:06:32     -620.131134        0.0059
BFGSLineSearch:    8[ 16] 16:06:34     -620.131137        0.0053
BFGSLineSearch:    9[ 17] 16:06:35     -620.131152        0.0030
BFGSLineSearch:   10[ 18] 16:06:36     -620.131158        0.0081
BFGSLineSearch:   11[ 20] 16:06:38     -620.131160        0.0026
BFGSLineSearch:   12[ 21] 16:06:39     -620.131163        0.0022
BFGSLineSearch:   13[ 22] 16:06:40     -620.131166        0.0019
BFGSLineSearch:   14[ 24] 16:06:42     -620.131167        0.0026
BFGSLineSearch:   15[ 26] 16:06:44     -620.131168        0.0025
BFGSLineSearch:   16[ 28] 16:06:46     -620.131169        0.0017
BFGSLineSearch:   17[ 29] 16:06:47     -620.131169        0.0010
BFGSLineSearch:   18[ 31] 16:06:49     -620.131169        0.0010
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__3
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  0.026666666666666665
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:06:51     -620.128988        0.2439
BFGSLineSearch:    1[  2] 16:06:53     -620.129881        0.0679
BFGSLineSearch:    2[  4] 16:06:55     -620.130350        0.0551
BFGSLineSearch:    3[  6] 16:06:56     -620.130502        0.0309
BFGSLineSearch:    4[  8] 16:06:58     -620.130596        0.0270
BFGSLineSearch:    5[ 10] 16:07:00     -620.130645        0.0202
BFGSLineSearch:    6[ 12] 16:07:02     -620.130708        0.0167
BFGSLineSearch:    7[ 14] 16:07:04     -620.130738        0.0117
BFGSLineSearch:    8[ 16] 16:07:07     -620.130747        0.0105
BFGSLineSearch:    9[ 17] 16:07:08     -620.130805        0.0063
BFGSLineSearch:   10[ 19] 16:07:11     -620.130818        0.0160
BFGSLineSearch:   11[ 21] 16:07:13     -620.130838        0.0052
BFGSLineSearch:   12[ 22] 16:07:14     -620.130849        0.0042
BFGSLineSearch:   13[ 23] 16:07:15     -620.130862        0.0038
BFGSLineSearch:   14[ 25] 16:07:17     -620.130867        0.0070
BFGSLineSearch:   15[ 27] 16:07:19     -620.130868        0.0046
BFGSLineSearch:   16[ 28] 16:07:20     -620.130872        0.0043
BFGSLineSearch:   17[ 29] 16:07:21     -620.130875        0.0026
BFGSLineSearch:   18[ 31] 16:07:23     -620.130875        0.0016
BFGSLineSearch:   19[ 32] 16:07:24     -620.130875        0.0005
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__4
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  -0.026666666666666665
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:07:26     -620.128987        0.2492
BFGSLineSearch:    1[  2] 16:07:28     -620.129880        0.0674
BFGSLineSearch:    2[  4] 16:07:29     -620.130348        0.0540
BFGSLineSearch:    3[  6] 16:07:31     -620.130496        0.0310
BFGSLineSearch:    4[  8] 16:07:33     -620.130594        0.0262
BFGSLineSearch:    5[ 10] 16:07:35     -620.130643        0.0208
BFGSLineSearch:    6[ 12] 16:07:37     -620.130708        0.0167
BFGSLineSearch:    7[ 14] 16:07:39     -620.130740        0.0106
BFGSLineSearch:    8[ 16] 16:07:41     -620.130750        0.0113
BFGSLineSearch:    9[ 17] 16:07:42     -620.130814        0.0062
BFGSLineSearch:   10[ 18] 16:07:43     -620.130838        0.0104
BFGSLineSearch:   11[ 20] 16:07:45     -620.130844        0.0065
BFGSLineSearch:   12[ 21] 16:07:46     -620.130859        0.0041
BFGSLineSearch:   13[ 22] 16:07:46     -620.130869        0.0034
BFGSLineSearch:   14[ 23] 16:07:47     -620.130874        0.0028
BFGSLineSearch:   15[ 25] 16:07:49     -620.130875        0.0061
BFGSLineSearch:   16[ 27] 16:07:51     -620.130879        0.0036
BFGSLineSearch:   17[ 28] 16:07:52     -620.130882        0.0018
BFGSLineSearch:   18[ 30] 16:07:54     -620.130883        0.0036
BFGSLineSearch:   19[ 32] 16:07:56     -620.130884        0.0006
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__5
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  0.04
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:07:58     -620.126156        0.3619
BFGSLineSearch:    1[  2] 16:08:00     -620.128145        0.1024
BFGSLineSearch:    2[  4] 16:08:02     -620.129205        0.0823
BFGSLineSearch:    3[  6] 16:08:04     -620.129544        0.0463
BFGSLineSearch:    4[  8] 16:08:06     -620.129756        0.0394
BFGSLineSearch:    5[ 10] 16:08:07     -620.129865        0.0304
BFGSLineSearch:    6[ 12] 16:08:09     -620.130003        0.0251
BFGSLineSearch:    7[ 14] 16:08:11     -620.130069        0.0182
BFGSLineSearch:    8[ 16] 16:08:13     -620.130090        0.0159
BFGSLineSearch:    9[ 17] 16:08:14     -620.130213        0.0103
BFGSLineSearch:   10[ 19] 16:08:16     -620.130242        0.0242
BFGSLineSearch:   11[ 21] 16:08:18     -620.130290        0.0078
BFGSLineSearch:   12[ 23] 16:08:20     -620.130323        0.0077
BFGSLineSearch:   13[ 24] 16:08:21     -620.130344        0.0056
BFGSLineSearch:   14[ 26] 16:08:23     -620.130357        0.0064
BFGSLineSearch:   15[ 28] 16:08:25     -620.130359        0.0086
BFGSLineSearch:   16[ 30] 16:08:28     -620.130367        0.0048
BFGSLineSearch:   17[ 31] 16:08:29     -620.130372        0.0037
BFGSLineSearch:   18[ 33] 16:08:32     -620.130373        0.0025
BFGSLineSearch:   19[ 34] 16:08:33     -620.130374        0.0007
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__6
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  -0.04
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:08:35     -620.126118        0.3776
BFGSLineSearch:    1[  2] 16:08:37     -620.128147        0.1015
BFGSLineSearch:    2[  4] 16:08:39     -620.129197        0.0816
BFGSLineSearch:    3[  6] 16:08:41     -620.129533        0.0467
BFGSLineSearch:    4[  8] 16:08:44     -620.129753        0.0400
BFGSLineSearch:    5[ 10] 16:08:46     -620.129865        0.0311
BFGSLineSearch:    6[ 12] 16:08:48     -620.130015        0.0251
BFGSLineSearch:    7[ 14] 16:08:50     -620.130089        0.0143
BFGSLineSearch:    8[ 16] 16:08:52     -620.130110        0.0190
BFGSLineSearch:    9[ 17] 16:08:53     -620.130264        0.0102
BFGSLineSearch:   10[ 18] 16:08:54     -620.130317        0.0076
BFGSLineSearch:   11[ 19] 16:08:55     -620.130341        0.0280
BFGSLineSearch:   12[ 21] 16:08:58     -620.130361        0.0082
BFGSLineSearch:   13[ 22] 16:08:59     -620.130382        0.0046
BFGSLineSearch:   14[ 23] 16:09:00     -620.130395        0.0046
BFGSLineSearch:   15[ 25] 16:09:02     -620.130399        0.0121
BFGSLineSearch:   16[ 27] 16:09:05     -620.130407        0.0051
BFGSLineSearch:   17[ 28] 16:09:06     -620.130414        0.0030
BFGSLineSearch:   18[ 30] 16:09:08     -620.130415        0.0057
BFGSLineSearch:   19[ 32] 16:09:10     -620.130417        0.0009
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__7
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  0.05333333333333333
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:09:12     -620.122206        0.4778
BFGSLineSearch:    1[  2] 16:09:14     -620.125713        0.1371
BFGSLineSearch:    2[  4] 16:09:16     -620.127603        0.1096
BFGSLineSearch:    3[  6] 16:09:18     -620.128204        0.0616
BFGSLineSearch:    4[  8] 16:09:20     -620.128581        0.0516
BFGSLineSearch:    5[ 10] 16:09:22     -620.128772        0.0406
BFGSLineSearch:    6[ 12] 16:09:24     -620.129012        0.0336
BFGSLineSearch:    7[ 14] 16:09:26     -620.129126        0.0249
BFGSLineSearch:    8[ 16] 16:09:28     -620.129165        0.0213
BFGSLineSearch:    9[ 17] 16:09:29     -620.129376        0.0147
BFGSLineSearch:   10[ 19] 16:09:30     -620.129425        0.0323
BFGSLineSearch:   11[ 21] 16:09:32     -620.129514        0.0104
BFGSLineSearch:   12[ 23] 16:09:34     -620.129573        0.0104
BFGSLineSearch:   13[ 24] 16:09:35     -620.129610        0.0075
BFGSLineSearch:   14[ 26] 16:09:37     -620.129632        0.0107
BFGSLineSearch:   15[ 28] 16:09:39     -620.129636        0.0093
BFGSLineSearch:   16[ 30] 16:09:41     -620.129651        0.0057
BFGSLineSearch:   17[ 31] 16:09:42     -620.129660        0.0028
BFGSLineSearch:   18[ 33] 16:09:44     -620.129661        0.0042
BFGSLineSearch:   19[ 34] 16:09:45     -620.129663        0.0024
BFGSLineSearch:   20[ 35] 16:09:46     -620.129663        0.0045
BFGSLineSearch:   21[ 37] 16:09:48     -620.129664        0.0013
BFGSLineSearch:   22[ 38] 16:09:49     -620.129664        0.0030
BFGSLineSearch:   23[ 39] 16:09:50     -620.129665        0.0050
BFGSLineSearch:   24[ 41] 16:09:52     -620.129666        0.0056
BFGSLineSearch:   25[ 43] 16:09:54     -620.129666        0.0030
BFGSLineSearch:   26[ 45] 16:09:56     -620.129667        0.0025
BFGSLineSearch:   27[ 48] 16:09:59     -620.129675        0.0061
BFGSLineSearch:   28[ 50] 16:10:01     -620.129693        0.0140
BFGSLineSearch:   29[ 51] 16:10:02     -620.129764        0.0244
BFGSLineSearch:   30[ 52] 16:10:03     -620.129809        0.0320
BFGSLineSearch:   31[ 54] 16:10:05     -620.129829        0.0087
BFGSLineSearch:   32[ 56] 16:10:07     -620.129857        0.0051
BFGSLineSearch:   33[ 57] 16:10:08     -620.129870        0.0033
BFGSLineSearch:   34[ 58] 16:10:09     -620.129881        0.0039
BFGSLineSearch:   35[ 59] 16:10:10     -620.129892        0.0101
BFGSLineSearch:   36[ 61] 16:10:12     -620.129909        0.0057
BFGSLineSearch:   37[ 62] 16:10:12     -620.129916        0.0053
BFGSLineSearch:   38[ 63] 16:10:13     -620.129921        0.0035
BFGSLineSearch:   39[ 65] 16:10:15     -620.129926        0.0053
BFGSLineSearch:   40[ 67] 16:10:17     -620.129931        0.0030
BFGSLineSearch:   41[ 68] 16:10:18     -620.129933        0.0014
BFGSLineSearch:   42[ 69] 16:10:19     -620.129934        0.0017
BFGSLineSearch:   43[ 71] 16:10:21     -620.129935        0.0011
BFGSLineSearch:   44[ 72] 16:10:22     -620.129935        0.0008
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__8
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  -0.05333333333333333
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:10:24     -620.122088        0.5081
BFGSLineSearch:    1[  2] 16:10:26     -620.125720        0.1356
BFGSLineSearch:    2[  4] 16:10:28     -620.127583        0.1093
BFGSLineSearch:    3[  6] 16:10:30     -620.128185        0.0625
BFGSLineSearch:    4[  8] 16:10:31     -620.128577        0.0541
BFGSLineSearch:    5[ 10] 16:10:33     -620.128778        0.0413
BFGSLineSearch:    6[ 12] 16:10:35     -620.129049        0.0336
BFGSLineSearch:    7[ 14] 16:10:37     -620.129183        0.0184
BFGSLineSearch:    8[ 16] 16:10:39     -620.129224        0.0286
BFGSLineSearch:    9[ 17] 16:10:40     -620.129510        0.0157
BFGSLineSearch:   10[ 18] 16:10:41     -620.129591        0.0137
BFGSLineSearch:   11[ 20] 16:10:43     -620.129619        0.0238
BFGSLineSearch:   12[ 21] 16:10:44     -620.129681        0.0138
BFGSLineSearch:   13[ 22] 16:10:45     -620.129715        0.0074
BFGSLineSearch:   14[ 23] 16:10:46     -620.129740        0.0044
BFGSLineSearch:   15[ 24] 16:10:47     -620.129754        0.0186
BFGSLineSearch:   16[ 26] 16:10:49     -620.129760        0.0078
BFGSLineSearch:   17[ 27] 16:10:50     -620.129772        0.0078
BFGSLineSearch:   18[ 29] 16:10:52     -620.129773        0.0045
BFGSLineSearch:   19[ 30] 16:10:53     -620.129777        0.0011
BFGSLineSearch:   20[ 32] 16:10:55     -620.129777        0.0025
BFGSLineSearch:   21[ 34] 16:10:57     -620.129778        0.0016
BFGSLineSearch:   22[ 36] 16:10:59     -620.129778        0.0024
BFGSLineSearch:   23[ 37] 16:11:00     -620.129779        0.0014
BFGSLineSearch:   24[ 39] 16:11:02     -620.129780        0.0020
BFGSLineSearch:   25[ 41] 16:11:04     -620.129780        0.0017
BFGSLineSearch:   26[ 43] 16:11:06     -620.129780        0.0012
BFGSLineSearch:   27[ 46] 16:11:09     -620.129788        0.0060
BFGSLineSearch:   28[ 47] 16:11:10     -620.129793        0.0056
BFGSLineSearch:   29[ 48] 16:11:11     -620.129802        0.0020
BFGSLineSearch:   30[ 50] 16:11:13     -620.129808        0.0045
BFGSLineSearch:   31[ 54] 16:11:17     -620.129871        0.0174
BFGSLineSearch:   32[ 56] 16:11:19     -620.129903        0.0340
BFGSLineSearch:   33[ 57] 16:11:20     -620.130059        0.0693
BFGSLineSearch:   34[ 59] 16:11:22     -620.130807        0.1192
BFGSLineSearch:   35[ 60] 16:11:23     -620.131616        0.0405
BFGSLineSearch:   36[ 62] 16:11:25     -620.132118        0.0656
BFGSLineSearch:   37[ 63] 16:11:26     -620.132866        0.0832
BFGSLineSearch:   38[ 65] 16:11:28     -620.133945        0.1154
BFGSLineSearch:   39[ 67] 16:11:30     -620.134531        0.1062
BFGSLineSearch:   40[ 68] 16:11:31     -620.135412        0.0278
BFGSLineSearch:   41[ 69] 16:11:33     -620.135612        0.0293
BFGSLineSearch:   42[ 70] 16:11:34     -620.135711        0.0404
BFGSLineSearch:   43[ 71] 16:11:35     -620.135799        0.0345
BFGSLineSearch:   44[ 72] 16:11:35     -620.135854        0.0155
BFGSLineSearch:   45[ 73] 16:11:36     -620.135868        0.0067
BFGSLineSearch:   46[ 74] 16:11:37     -620.135873        0.0032
BFGSLineSearch:   47[ 75] 16:11:38     -620.135875        0.0034
BFGSLineSearch:   48[ 76] 16:11:39     -620.135877        0.0024
BFGSLineSearch:   49[ 77] 16:11:40     -620.135878        0.0016
BFGSLineSearch:   50[ 78] 16:11:41     -620.135878        0.0003
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__9
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  0.06666666666666667
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:11:43     -620.117145        0.5915
BFGSLineSearch:    1[  2] 16:11:45     -620.122582        0.1721
BFGSLineSearch:    2[  4] 16:11:47     -620.125547        0.1398
BFGSLineSearch:    3[  6] 16:11:49     -620.126483        0.0769
BFGSLineSearch:    4[  8] 16:11:51     -620.127071        0.0634
BFGSLineSearch:    5[ 10] 16:11:53     -620.127366        0.0507
BFGSLineSearch:    6[ 12] 16:11:55     -620.127735        0.0421
BFGSLineSearch:    7[ 14] 16:11:56     -620.127908        0.0317
BFGSLineSearch:    8[ 16] 16:11:58     -620.127971        0.0267
BFGSLineSearch:    9[ 18] 16:12:00     -620.128377        0.0314
BFGSLineSearch:   10[ 20] 16:12:02     -620.128404        0.0257
BFGSLineSearch:   11[ 21] 16:12:03     -620.128546        0.0238
BFGSLineSearch:   12[ 22] 16:12:04     -620.128614        0.0134
BFGSLineSearch:   13[ 24] 16:12:06     -620.128668        0.0210
BFGSLineSearch:   14[ 25] 16:12:07     -620.128693        0.0124
BFGSLineSearch:   15[ 26] 16:12:08     -620.128716        0.0247
BFGSLineSearch:   16[ 28] 16:12:10     -620.128724        0.0165
BFGSLineSearch:   17[ 29] 16:12:11     -620.128735        0.0082
BFGSLineSearch:   18[ 31] 16:12:13     -620.128736        0.0062
BFGSLineSearch:   19[ 32] 16:12:14     -620.128738        0.0036
BFGSLineSearch:   20[ 34] 16:12:16     -620.128738        0.0037
BFGSLineSearch:   21[ 35] 16:12:17     -620.128740        0.0059
BFGSLineSearch:   22[ 37] 16:12:19     -620.128740        0.0057
BFGSLineSearch:   23[ 39] 16:12:21     -620.128742        0.0025
BFGSLineSearch:   24[ 41] 16:12:23     -620.128743        0.0024
BFGSLineSearch:   25[ 43] 16:12:25     -620.128744        0.0032
BFGSLineSearch:   26[ 44] 16:12:26     -620.128746        0.0037
BFGSLineSearch:   27[ 45] 16:12:27     -620.128750        0.0039
BFGSLineSearch:   28[ 47] 16:12:29     -620.128800        0.0178
BFGSLineSearch:   29[ 49] 16:12:31     -620.128854        0.0260
BFGSLineSearch:   30[ 50] 16:12:32     -620.128873        0.0069
BFGSLineSearch:   31[ 52] 16:12:34     -620.128886        0.0073
BFGSLineSearch:   32[ 54] 16:12:36     -620.128903        0.0129
BFGSLineSearch:   33[ 56] 16:12:38     -620.128918        0.0045
BFGSLineSearch:   34[ 57] 16:12:39     -620.128931        0.0071
BFGSLineSearch:   35[ 58] 16:12:40     -620.128937        0.0054
BFGSLineSearch:   36[ 59] 16:12:41     -620.128942        0.0022
BFGSLineSearch:   37[ 60] 16:12:42     -620.128946        0.0033
BFGSLineSearch:   38[ 61] 16:12:42     -620.128952        0.0032
BFGSLineSearch:   39[ 62] 16:12:43     -620.128954        0.0018
BFGSLineSearch:   40[ 63] 16:12:44     -620.128955        0.0012
BFGSLineSearch:   41[ 64] 16:12:45     -620.128956        0.0012
BFGSLineSearch:   42[ 65] 16:12:46     -620.128957        0.0011
BFGSLineSearch:   43[ 66] 16:12:47     -620.128957        0.0010
BFGSLineSearch:   44[ 67] 16:12:48     -620.128958        0.0004
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__10
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  -0.06666666666666667
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:12:50     -620.116892        0.6407
BFGSLineSearch:    1[  2] 16:12:52     -620.122601        0.1704
BFGSLineSearch:    2[  4] 16:12:54     -620.125506        0.1372
BFGSLineSearch:    3[  6] 16:12:56     -620.126451        0.0783
BFGSLineSearch:    4[  8] 16:12:58     -620.127066        0.0685
BFGSLineSearch:    5[ 10] 16:13:00     -620.127385        0.0516
BFGSLineSearch:    6[ 12] 16:13:01     -620.127815        0.0420
BFGSLineSearch:    7[ 14] 16:13:03     -620.128028        0.0224
BFGSLineSearch:    8[ 16] 16:13:05     -620.128098        0.0409
BFGSLineSearch:    9[ 17] 16:13:06     -620.128547        0.0239
BFGSLineSearch:   10[ 18] 16:13:07     -620.128653        0.0295
BFGSLineSearch:   11[ 20] 16:13:09     -620.128731        0.0191
BFGSLineSearch:   12[ 21] 16:13:10     -620.128833        0.0120
BFGSLineSearch:   13[ 23] 16:13:12     -620.128898        0.0099
BFGSLineSearch:   14[ 24] 16:13:13     -620.128925        0.0129
BFGSLineSearch:   15[ 26] 16:13:15     -620.128931        0.0141
BFGSLineSearch:   16[ 27] 16:13:16     -620.128958        0.0138
BFGSLineSearch:   17[ 28] 16:13:17     -620.128971        0.0031
BFGSLineSearch:   18[ 30] 16:13:19     -620.128972        0.0054
BFGSLineSearch:   19[ 32] 16:13:21     -620.128973        0.0016
BFGSLineSearch:   20[ 34] 16:13:23     -620.128973        0.0019
BFGSLineSearch:   21[ 36] 16:13:25     -620.128976        0.0049
BFGSLineSearch:   22[ 38] 16:13:28     -620.128976        0.0022
BFGSLineSearch:   23[ 40] 16:13:30     -620.128977        0.0019
BFGSLineSearch:   24[ 42] 16:13:32     -620.128977        0.0037
BFGSLineSearch:   25[ 44] 16:13:34     -620.128978        0.0016
BFGSLineSearch:   26[ 46] 16:13:36     -620.128983        0.0038
BFGSLineSearch:   27[ 47] 16:13:37     -620.128992        0.0039
BFGSLineSearch:   28[ 48] 16:13:38     -620.128995        0.0023
BFGSLineSearch:   29[ 49] 16:13:39     -620.128998        0.0017
BFGSLineSearch:   30[ 51] 16:13:41     -620.129002        0.0051
BFGSLineSearch:   31[ 55] 16:13:45     -620.129070        0.0184
BFGSLineSearch:   32[ 57] 16:13:47     -620.129121        0.0463
BFGSLineSearch:   33[ 58] 16:13:48     -620.129257        0.0896
BFGSLineSearch:   34[ 60] 16:13:50     -620.129903        0.1102
BFGSLineSearch:   35[ 61] 16:13:51     -620.130743        0.0330
BFGSLineSearch:   36[ 63] 16:13:53     -620.131161        0.0729
BFGSLineSearch:   37[ 64] 16:13:55     -620.132076        0.0826
BFGSLineSearch:   38[ 66] 16:13:57     -620.133527        0.1531
BFGSLineSearch:   39[ 67] 16:13:58     -620.134461        0.1526
BFGSLineSearch:   40[ 69] 16:14:00     -620.135103        0.1023
BFGSLineSearch:   41[ 70] 16:14:01     -620.135808        0.0851
BFGSLineSearch:   42[ 71] 16:14:02     -620.136155        0.0297
BFGSLineSearch:   43[ 72] 16:14:03     -620.136379        0.0385
BFGSLineSearch:   44[ 73] 16:14:04     -620.136502        0.0240
BFGSLineSearch:   45[ 74] 16:14:05     -620.136560        0.0166
BFGSLineSearch:   46[ 75] 16:14:06     -620.136583        0.0091
BFGSLineSearch:   47[ 76] 16:14:07     -620.136594        0.0049
BFGSLineSearch:   48[ 77] 16:14:08     -620.136600        0.0055
BFGSLineSearch:   49[ 78] 16:14:09     -620.136603        0.0040
BFGSLineSearch:   50[ 79] 16:14:10     -620.136604        0.0020
BFGSLineSearch:   51[ 80] 16:14:11     -620.136604        0.0003
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__11
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  0.08
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:14:13     -620.110982        0.7032
BFGSLineSearch:    1[  2] 16:14:15     -620.118752        0.2073
BFGSLineSearch:    2[  4] 16:14:17     -620.123036        0.1713
BFGSLineSearch:    3[  6] 16:14:18     -620.124381        0.0921
BFGSLineSearch:    4[  8] 16:14:20     -620.125227        0.0750
BFGSLineSearch:    5[ 10] 16:14:22     -620.125648        0.0607
BFGSLineSearch:    6[ 12] 16:14:24     -620.126169        0.0506
BFGSLineSearch:    7[ 14] 16:14:26     -620.126412        0.0385
BFGSLineSearch:    8[ 16] 16:14:28     -620.126505        0.0322
BFGSLineSearch:    9[ 18] 16:14:30     -620.127079        0.0415
BFGSLineSearch:   10[ 20] 16:14:32     -620.127116        0.0305
BFGSLineSearch:   11[ 21] 16:14:33     -620.127321        0.0283
BFGSLineSearch:   12[ 22] 16:14:34     -620.127416        0.0177
BFGSLineSearch:   13[ 23] 16:14:35     -620.127477        0.0111
BFGSLineSearch:   14[ 24] 16:14:36     -620.127527        0.0188
BFGSLineSearch:   15[ 26] 16:14:38     -620.127534        0.0223
BFGSLineSearch:   16[ 28] 16:14:40     -620.127562        0.0109
BFGSLineSearch:   17[ 29] 16:14:41     -620.127586        0.0038
BFGSLineSearch:   18[ 31] 16:14:42     -620.127589        0.0096
BFGSLineSearch:   19[ 33] 16:14:44     -620.127595        0.0023
BFGSLineSearch:   20[ 35] 16:14:46     -620.127595        0.0039
BFGSLineSearch:   21[ 36] 16:14:47     -620.127599        0.0042
BFGSLineSearch:   22[ 38] 16:14:49     -620.127600        0.0054
BFGSLineSearch:   23[ 40] 16:14:51     -620.127601        0.0058
BFGSLineSearch:   24[ 42] 16:14:53     -620.127603        0.0031
BFGSLineSearch:   25[ 44] 16:14:55     -620.127606        0.0103
BFGSLineSearch:   26[ 46] 16:14:57     -620.127611        0.0147
BFGSLineSearch:   27[ 47] 16:14:58     -620.127621        0.0160
BFGSLineSearch:   28[ 50] 16:15:01     -620.127697        0.0154
BFGSLineSearch:   29[ 51] 16:15:02     -620.127719        0.0092
BFGSLineSearch:   30[ 52] 16:15:03     -620.127727        0.0045
BFGSLineSearch:   31[ 53] 16:15:04     -620.127733        0.0040
BFGSLineSearch:   32[ 55] 16:15:06     -620.127747        0.0045
BFGSLineSearch:   33[ 56] 16:15:07     -620.127759        0.0118
BFGSLineSearch:   34[ 57] 16:15:08     -620.127773        0.0065
BFGSLineSearch:   35[ 58] 16:15:09     -620.127780        0.0021
BFGSLineSearch:   36[ 60] 16:15:11     -620.127786        0.0089
BFGSLineSearch:   37[ 62] 16:15:13     -620.127792        0.0038
BFGSLineSearch:   38[ 63] 16:15:14     -620.127794        0.0012
BFGSLineSearch:   39[ 65] 16:15:16     -620.127795        0.0011
BFGSLineSearch:   40[ 67] 16:15:18     -620.127796        0.0017
BFGSLineSearch:   41[ 68] 16:15:19     -620.127796        0.0011
BFGSLineSearch:   42[ 69] 16:15:20     -620.127796        0.0002
Pattern Name =  UREAXX12_DFTB3D3H5_cij__1__12
Pattern =  [1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
Magnitude =  -0.08
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:15:21     -620.110521        0.7753
BFGSLineSearch:    1[  2] 16:15:23     -620.118790        0.2087
BFGSLineSearch:    2[  4] 16:15:25     -620.122964        0.1651
BFGSLineSearch:    3[  6] 16:15:27     -620.124332        0.0941
BFGSLineSearch:    4[  8] 16:15:29     -620.125221        0.0832
BFGSLineSearch:    5[ 10] 16:15:31     -620.125684        0.0619
BFGSLineSearch:    6[ 12] 16:15:33     -620.126313        0.0505
BFGSLineSearch:    7[ 14] 16:15:35     -620.126625        0.0273
BFGSLineSearch:    8[ 16] 16:15:37     -620.126741        0.0569
BFGSLineSearch:    9[ 17] 16:15:38     -620.127328        0.0366
BFGSLineSearch:   10[ 18] 16:15:39     -620.127484        0.0580
BFGSLineSearch:   11[ 20] 16:15:41     -620.127621        0.0208
BFGSLineSearch:   12[ 21] 16:15:41     -620.127763        0.0146
BFGSLineSearch:   13[ 22] 16:15:42     -620.127861        0.0110
BFGSLineSearch:   14[ 23] 16:15:43     -620.127917        0.0077
BFGSLineSearch:   15[ 24] 16:15:44     -620.127945        0.0356
BFGSLineSearch:   16[ 26] 16:15:46     -620.127965        0.0128
BFGSLineSearch:   17[ 27] 16:15:47     -620.127997        0.0062
BFGSLineSearch:   18[ 29] 16:15:49     -620.128000        0.0110
BFGSLineSearch:   19[ 31] 16:15:51     -620.128010        0.0033
BFGSLineSearch:   20[ 32] 16:15:52     -620.128011        0.0051
BFGSLineSearch:   21[ 34] 16:15:54     -620.128012        0.0037
BFGSLineSearch:   22[ 35] 16:15:56     -620.128013        0.0063
BFGSLineSearch:   23[ 37] 16:15:58     -620.128016        0.0046
BFGSLineSearch:   24[ 39] 16:16:00     -620.128017        0.0049
BFGSLineSearch:   25[ 41] 16:16:02     -620.128018        0.0041
BFGSLineSearch:   26[ 43] 16:16:04     -620.128019        0.0026
BFGSLineSearch:   27[ 46] 16:16:07     -620.128032        0.0059
BFGSLineSearch:   28[ 47] 16:16:08     -620.128037        0.0049
BFGSLineSearch:   29[ 49] 16:16:10     -620.128040        0.0014
BFGSLineSearch:   30[ 51] 16:16:12     -620.128042        0.0044
BFGSLineSearch:   31[ 53] 16:16:14     -620.128055        0.0113
BFGSLineSearch:   32[ 55] 16:16:16     -620.128239        0.0533
BFGSLineSearch:   33[ 57] 16:16:18     -620.128385        0.0785
BFGSLineSearch:   34[ 59] 16:16:20     -620.128950        0.0815
BFGSLineSearch:   35[ 60] 16:16:21     -620.129530        0.0882
BFGSLineSearch:   36[ 62] 16:16:23     -620.130262        0.0828
BFGSLineSearch:   37[ 65] 16:16:26     -620.130935        0.1125
BFGSLineSearch:   38[ 68] 16:16:30     -620.133233        0.1193
BFGSLineSearch:   39[ 70] 16:16:32     -620.134821        0.1012
BFGSLineSearch:   40[ 71] 16:16:33     -620.135743        0.0672
BFGSLineSearch:   41[ 72] 16:16:34     -620.136340        0.0755
BFGSLineSearch:   42[ 73] 16:16:35     -620.136692        0.0350
BFGSLineSearch:   43[ 74] 16:16:36     -620.137053        0.0595
BFGSLineSearch:   44[ 75] 16:16:37     -620.137178        0.0203
BFGSLineSearch:   45[ 76] 16:16:38     -620.137221        0.0232
BFGSLineSearch:   46[ 77] 16:16:39     -620.137250        0.0162
BFGSLineSearch:   47[ 78] 16:16:40     -620.137272        0.0077
BFGSLineSearch:   48[ 79] 16:16:41     -620.137281        0.0051
BFGSLineSearch:   49[ 80] 16:16:42     -620.137282        0.0039
BFGSLineSearch:   50[ 81] 16:16:43     -620.137283        0.0016
BFGSLineSearch:   51[ 82] 16:16:44     -620.137283        0.0004
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__1
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  0.013333333333333332
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:16:46     -620.128818        0.2252
BFGSLineSearch:    1[  2] 16:16:48     -620.130024        0.1238
BFGSLineSearch:    2[  4] 16:16:50     -620.130482        0.0313
BFGSLineSearch:    3[  6] 16:16:52     -620.130522        0.0191
BFGSLineSearch:    4[  7] 16:16:53     -620.130563        0.0338
BFGSLineSearch:    5[  9] 16:16:55     -620.130585        0.0209
BFGSLineSearch:    6[ 10] 16:16:56     -620.130652        0.0203
BFGSLineSearch:    7[ 12] 16:16:58     -620.130662        0.0133
BFGSLineSearch:    8[ 13] 16:16:59     -620.130695        0.0091
BFGSLineSearch:    9[ 15] 16:17:01     -620.130704        0.0108
BFGSLineSearch:   10[ 17] 16:17:03     -620.130708        0.0077
BFGSLineSearch:   11[ 18] 16:17:04     -620.130713        0.0096
BFGSLineSearch:   12[ 20] 16:17:06     -620.130714        0.0046
BFGSLineSearch:   13[ 21] 16:17:07     -620.130719        0.0029
BFGSLineSearch:   14[ 23] 16:17:09     -620.130728        0.0065
BFGSLineSearch:   15[ 24] 16:17:10     -620.130732        0.0033
BFGSLineSearch:   16[ 26] 16:17:13     -620.130732        0.0033
BFGSLineSearch:   17[ 28] 16:17:15     -620.130733        0.0001
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__2
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  -0.013333333333333332
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:17:17     -620.128810        0.2268
BFGSLineSearch:    1[  2] 16:17:19     -620.130003        0.1285
BFGSLineSearch:    2[  4] 16:17:21     -620.130469        0.0343
BFGSLineSearch:    3[  6] 16:17:23     -620.130507        0.0192
BFGSLineSearch:    4[  7] 16:17:24     -620.130554        0.0347
BFGSLineSearch:    5[  9] 16:17:26     -620.130577        0.0197
BFGSLineSearch:    6[ 10] 16:17:27     -620.130648        0.0239
BFGSLineSearch:    7[ 12] 16:17:29     -620.130658        0.0153
BFGSLineSearch:    8[ 13] 16:17:30     -620.130694        0.0064
BFGSLineSearch:    9[ 15] 16:17:32     -620.130702        0.0123
BFGSLineSearch:   10[ 17] 16:17:34     -620.130705        0.0073
BFGSLineSearch:   11[ 19] 16:17:36     -620.130711        0.0039
BFGSLineSearch:   12[ 21] 16:17:39     -620.130712        0.0039
BFGSLineSearch:   13[ 22] 16:17:40     -620.130717        0.0048
BFGSLineSearch:   14[ 24] 16:17:42     -620.130730        0.0028
BFGSLineSearch:   15[ 25] 16:17:43     -620.130731        0.0030
BFGSLineSearch:   16[ 27] 16:17:45     -620.130731        0.0002
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__3
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  0.026666666666666665
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:17:47     -620.121544        0.4419
BFGSLineSearch:    1[  2] 16:17:49     -620.126332        0.2395
BFGSLineSearch:    2[  4] 16:17:51     -620.128152        0.0616
BFGSLineSearch:    3[  6] 16:17:53     -620.128311        0.0386
BFGSLineSearch:    4[  8] 16:17:55     -620.128514        0.0421
BFGSLineSearch:    5[ 10] 16:17:57     -620.128584        0.0298
BFGSLineSearch:    6[ 11] 16:17:58     -620.128836        0.0402
BFGSLineSearch:    7[ 13] 16:18:01     -620.128876        0.0202
BFGSLineSearch:    8[ 14] 16:18:02     -620.128987        0.0247
BFGSLineSearch:    9[ 16] 16:18:04     -620.129024        0.0160
BFGSLineSearch:   10[ 18] 16:18:06     -620.129045        0.0121
BFGSLineSearch:   11[ 20] 16:18:08     -620.129053        0.0133
BFGSLineSearch:   12[ 22] 16:18:10     -620.129063        0.0086
BFGSLineSearch:   13[ 24] 16:18:12     -620.129092        0.0134
BFGSLineSearch:   14[ 26] 16:18:14     -620.129128        0.0020
BFGSLineSearch:   15[ 27] 16:18:15     -620.129129        0.0028
BFGSLineSearch:   16[ 29] 16:18:17     -620.129129        0.0005
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__4
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  -0.026666666666666665
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:18:19     -620.121364        0.4623
BFGSLineSearch:    1[  2] 16:18:21     -620.126169        0.2582
BFGSLineSearch:    2[  4] 16:18:24     -620.128042        0.0684
BFGSLineSearch:    3[  6] 16:18:26     -620.128197        0.0384
BFGSLineSearch:    4[  7] 16:18:27     -620.128401        0.0687
BFGSLineSearch:    5[  9] 16:18:29     -620.128492        0.0375
BFGSLineSearch:    6[ 10] 16:18:30     -620.128779        0.0489
BFGSLineSearch:    7[ 12] 16:18:32     -620.128819        0.0315
BFGSLineSearch:    8[ 13] 16:18:33     -620.128971        0.0130
BFGSLineSearch:    9[ 15] 16:18:35     -620.129001        0.0233
BFGSLineSearch:   10[ 17] 16:18:37     -620.129013        0.0162
BFGSLineSearch:   11[ 19] 16:18:39     -620.129036        0.0063
BFGSLineSearch:   12[ 21] 16:18:41     -620.129042        0.0141
BFGSLineSearch:   13[ 23] 16:18:43     -620.129058        0.0099
BFGSLineSearch:   14[ 25] 16:18:45     -620.129118        0.0020
BFGSLineSearch:   15[ 27] 16:18:48     -620.129118        0.0045
BFGSLineSearch:   16[ 29] 16:18:50     -620.129118        0.0003
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__5
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  0.04
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:18:52     -620.109526        0.6529
BFGSLineSearch:    1[  2] 16:18:54     -620.120241        0.3487
BFGSLineSearch:    2[  4] 16:18:56     -620.124313        0.0946
BFGSLineSearch:    3[  6] 16:18:58     -620.124675        0.0581
BFGSLineSearch:    4[  8] 16:19:00     -620.125117        0.0641
BFGSLineSearch:    5[ 10] 16:19:02     -620.125272        0.0438
BFGSLineSearch:    6[ 11] 16:19:03     -620.125801        0.0595
BFGSLineSearch:    7[ 13] 16:19:05     -620.125891        0.0315
BFGSLineSearch:    8[ 14] 16:19:06     -620.126142        0.0417
BFGSLineSearch:    9[ 16] 16:19:08     -620.126223        0.0250
BFGSLineSearch:   10[ 18] 16:19:10     -620.126276        0.0210
BFGSLineSearch:   11[ 20] 16:19:12     -620.126293        0.0206
BFGSLineSearch:   12[ 22] 16:19:15     -620.126318        0.0123
BFGSLineSearch:   13[ 24] 16:19:17     -620.126370        0.0176
BFGSLineSearch:   14[ 26] 16:19:19     -620.126457        0.0053
BFGSLineSearch:   15[ 27] 16:19:20     -620.126459        0.0038
BFGSLineSearch:   16[ 29] 16:19:22     -620.126460        0.0007
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__6
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  -0.04
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:19:24     -620.108846        0.7041
BFGSLineSearch:    1[  2] 16:19:26     -620.119699        0.3903
BFGSLineSearch:    2[  4] 16:19:28     -620.123944        0.1036
BFGSLineSearch:    3[  6] 16:19:30     -620.124295        0.0576
BFGSLineSearch:    4[  7] 16:19:31     -620.124786        0.1023
BFGSLineSearch:    5[  9] 16:19:33     -620.124996        0.0626
BFGSLineSearch:    6[ 10] 16:19:34     -620.125640        0.0755
BFGSLineSearch:    7[ 12] 16:19:37     -620.125733        0.0490
BFGSLineSearch:    8[ 13] 16:19:38     -620.126083        0.0207
BFGSLineSearch:    9[ 15] 16:19:40     -620.126154        0.0328
BFGSLineSearch:   10[ 17] 16:19:42     -620.126194        0.0249
BFGSLineSearch:   11[ 19] 16:19:44     -620.126231        0.0086
BFGSLineSearch:   12[ 20] 16:19:45     -620.126268        0.0115
BFGSLineSearch:   13[ 22] 16:19:47     -620.126287        0.0262
BFGSLineSearch:   14[ 23] 16:19:48     -620.126402        0.0129
BFGSLineSearch:   15[ 24] 16:19:49     -620.126426        0.0037
BFGSLineSearch:   16[ 26] 16:19:51     -620.126426        0.0066
BFGSLineSearch:   17[ 28] 16:19:53     -620.126427        0.0005
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__7
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  0.05333333333333333
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:19:55     -620.092842        0.8580
BFGSLineSearch:    1[  2] 16:19:58     -620.111801        0.4523
BFGSLineSearch:    2[  4] 16:20:00     -620.119004        0.1292
BFGSLineSearch:    3[  6] 16:20:02     -620.119653        0.0778
BFGSLineSearch:    4[  8] 16:20:04     -620.120416        0.0867
BFGSLineSearch:    5[ 10] 16:20:06     -620.120687        0.0573
BFGSLineSearch:    6[ 11] 16:20:07     -620.121563        0.0770
BFGSLineSearch:    7[ 13] 16:20:09     -620.121722        0.0454
BFGSLineSearch:    8[ 14] 16:20:10     -620.122167        0.0618
BFGSLineSearch:    9[ 16] 16:20:12     -620.122310        0.0339
BFGSLineSearch:   10[ 18] 16:20:14     -620.122413        0.0307
BFGSLineSearch:   11[ 20] 16:20:17     -620.122444        0.0284
BFGSLineSearch:   12[ 22] 16:20:19     -620.122494        0.0152
BFGSLineSearch:   13[ 24] 16:20:21     -620.122571        0.0208
BFGSLineSearch:   14[ 26] 16:20:23     -620.122729        0.0081
BFGSLineSearch:   15[ 27] 16:20:24     -620.122733        0.0013
BFGSLineSearch:   16[ 29] 16:20:26     -620.122733        0.0015
BFGSLineSearch:   17[ 31] 16:20:28     -620.122734        0.0005
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__8
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  -0.05333333333333333
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:20:30     -620.091175        0.9522
BFGSLineSearch:    1[  2] 16:20:32     -620.110532        0.5265
BFGSLineSearch:    2[  4] 16:20:34     -620.118134        0.1413
BFGSLineSearch:    3[  6] 16:20:36     -620.118761        0.0768
BFGSLineSearch:    4[  7] 16:20:38     -620.119690        0.1356
BFGSLineSearch:    5[  9] 16:20:40     -620.120075        0.0922
BFGSLineSearch:    6[ 10] 16:20:41     -620.121195        0.1029
BFGSLineSearch:    7[ 12] 16:20:43     -620.121374        0.0778
BFGSLineSearch:    8[ 13] 16:20:44     -620.121964        0.0342
BFGSLineSearch:    9[ 14] 16:20:45     -620.122136        0.0489
BFGSLineSearch:   10[ 16] 16:20:47     -620.122236        0.0287
BFGSLineSearch:   11[ 18] 16:20:49     -620.122269        0.0169
BFGSLineSearch:   12[ 19] 16:20:50     -620.122340        0.0179
BFGSLineSearch:   13[ 21] 16:20:52     -620.122377        0.0246
BFGSLineSearch:   14[ 22] 16:20:53     -620.122528        0.0194
BFGSLineSearch:   15[ 23] 16:20:55     -620.122639        0.0075
BFGSLineSearch:   16[ 25] 16:20:57     -620.122645        0.0215
BFGSLineSearch:   17[ 27] 16:20:59     -620.122654        0.0010
BFGSLineSearch:   18[ 29] 16:21:01     -620.122654        0.0007
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__9
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  0.06666666666666667
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:21:03     -620.071570        1.0574
BFGSLineSearch:    1[  2] 16:21:05     -620.101063        0.5515
BFGSLineSearch:    2[  4] 16:21:07     -620.112261        0.1649
BFGSLineSearch:    3[  6] 16:21:09     -620.113289        0.0998
BFGSLineSearch:    4[  8] 16:21:11     -620.114449        0.1099
BFGSLineSearch:    5[ 10] 16:21:13     -620.114864        0.0700
BFGSLineSearch:    6[ 11] 16:21:14     -620.116138        0.0919
BFGSLineSearch:    7[ 13] 16:21:17     -620.116380        0.0620
BFGSLineSearch:    8[ 14] 16:21:18     -620.117071        0.0850
BFGSLineSearch:    9[ 16] 16:21:20     -620.117297        0.0457
BFGSLineSearch:   10[ 18] 16:21:22     -620.117470        0.0408
BFGSLineSearch:   11[ 20] 16:21:24     -620.117518        0.0366
BFGSLineSearch:   12[ 22] 16:21:26     -620.117606        0.0177
BFGSLineSearch:   13[ 24] 16:21:28     -620.117709        0.0253
BFGSLineSearch:   14[ 26] 16:21:30     -620.117953        0.0087
BFGSLineSearch:   15[ 27] 16:21:31     -620.117960        0.0050
BFGSLineSearch:   16[ 29] 16:21:33     -620.117961        0.0019
BFGSLineSearch:   17[ 30] 16:21:34     -620.117961        0.0005
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__10
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  -0.06666666666666667
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:21:36     -620.068269        1.2066
BFGSLineSearch:    1[  2] 16:21:38     -620.098600        0.6688
BFGSLineSearch:    2[  4] 16:21:40     -620.110562        0.1832
BFGSLineSearch:    3[  6] 16:21:42     -620.111548        0.0958
BFGSLineSearch:    4[  7] 16:21:43     -620.113086        0.1685
BFGSLineSearch:    5[  9] 16:21:46     -620.113709        0.1260
BFGSLineSearch:    6[ 11] 16:21:48     -620.115881        0.0795
BFGSLineSearch:    7[ 13] 16:21:50     -620.116046        0.0986
BFGSLineSearch:    8[ 14] 16:21:51     -620.116925        0.0294
BFGSLineSearch:    9[ 16] 16:21:53     -620.117073        0.0218
BFGSLineSearch:   10[ 18] 16:21:55     -620.117123        0.0387
BFGSLineSearch:   11[ 20] 16:21:57     -620.117203        0.0258
BFGSLineSearch:   12[ 21] 16:21:58     -620.117316        0.0863
BFGSLineSearch:   13[ 23] 16:22:00     -620.117543        0.0437
BFGSLineSearch:   14[ 24] 16:22:02     -620.117758        0.0164
BFGSLineSearch:   15[ 25] 16:22:03     -620.117789        0.0019
BFGSLineSearch:   16[ 27] 16:22:05     -620.117789        0.0048
BFGSLineSearch:   17[ 29] 16:22:07     -620.117791        0.0015
BFGSLineSearch:   18[ 30] 16:22:08     -620.117791        0.0007
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__11
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  0.08
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:22:10     -620.045792        1.2509
BFGSLineSearch:    1[  2] 16:22:12     -620.088083        0.6472
BFGSLineSearch:    2[  4] 16:22:13     -620.104129        0.2016
BFGSLineSearch:    3[  6] 16:22:15     -620.105629        0.1241
BFGSLineSearch:    4[  8] 16:22:17     -620.107255        0.1337
BFGSLineSearch:    5[ 10] 16:22:19     -620.107844        0.0816
BFGSLineSearch:    6[ 11] 16:22:20     -620.109545        0.1175
BFGSLineSearch:    7[ 13] 16:22:21     -620.109886        0.0788
BFGSLineSearch:    8[ 14] 16:22:22     -620.110861        0.1114
BFGSLineSearch:    9[ 16] 16:22:24     -620.111191        0.0584
BFGSLineSearch:   10[ 18] 16:22:26     -620.111455        0.0512
BFGSLineSearch:   11[ 20] 16:22:27     -620.111527        0.0450
BFGSLineSearch:   12[ 22] 16:22:29     -620.111666        0.0198
BFGSLineSearch:   13[ 24] 16:22:31     -620.111798        0.0286
BFGSLineSearch:   14[ 26] 16:22:33     -620.112141        0.0079
BFGSLineSearch:   15[ 27] 16:22:34     -620.112151        0.0021
BFGSLineSearch:   16[ 29] 16:22:35     -620.112151        0.0037
BFGSLineSearch:   17[ 31] 16:22:37     -620.112152        0.0004
Pattern Name =  UREAXX12_DFTB3D3H5_cij__2__12
Pattern =  [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
Magnitude =  -0.08
                Step[ FC]     Time          Energy          fmax
BFGSLineSearch:    0[  0] 16:22:39     -620.040047        1.4675
BFGSLineSearch:    1[  2] 16:22:41     -620.083827        0.8192
BFGSLineSearch:    2[  4] 16:22:43     -620.101176        0.2310
BFGSLineSearch:    3[  6] 16:22:45     -620.102606        0.1147
BFGSLineSearch:    4[  7] 16:22:46     -620.104947        0.2011
BFGSLineSearch:    5[  9] 16:22:49     -620.105879        0.1629
BFGSLineSearch:    6[ 11] 16:22:51     -620.109068        0.0901
BFGSLineSearch:    7[ 13] 16:22:53     -620.109302        0.1405
BFGSLineSearch:    8[ 14] 16:22:54     -620.110580        0.0520
BFGSLineSearch:    9[ 16] 16:22:56     -620.110754        0.0401
BFGSLineSearch:   10[ 18] 16:22:58     -620.110811        0.0508
BFGSLineSearch:   11[ 20] 16:23:00     -620.110959        0.0350
BFGSLineSearch:   12[ 22] 16:23:02     -620.111067        0.0662
BFGSLineSearch:   13[ 23] 16:23:03     -620.111420        0.1281
BFGSLineSearch:   14[ 24] 16:23:04     -620.111782        0.0321
BFGSLineSearch:   15[ 25] 16:23:05     -620.111832        0.0051
BFGSLineSearch:   16[ 27] 16:23:08     -620.111833        0.0029
BFGSLineSearch:   17[ 28] 16:23:09     -620.111836        0.0018
BFGSLineSearch:   18[ 29] 16:23:10     -620.111838        0.0041
BFGSLineSearch:   19[ 31] 16:23:12     -620.111838        0.0018
BFGSLineSearch:   20[ 32] 16:23:13     -620.111838        0.0017
BFGSLineSearch:   21[ 34] 16:23:15     -620.111838        0.0005

Reading strain data from  UREAXX12_DFTB3D3H5.cijdat

24.0
System is Tetragonal 

12 steps of maximum magnitude 0.08

Analysing pattern 1 :
UREAXX12_DFTB3D3H5_cij__1__1

0
UREAXX12_DFTB3D3H5_cij__1__2

1
UREAXX12_DFTB3D3H5_cij__1__3

2
UREAXX12_DFTB3D3H5_cij__1__4

3
UREAXX12_DFTB3D3H5_cij__1__5

4
UREAXX12_DFTB3D3H5_cij__1__6

5
UREAXX12_DFTB3D3H5_cij__1__7

6
UREAXX12_DFTB3D3H5_cij__1__8

7
UREAXX12_DFTB3D3H5_cij__1__9

8
UREAXX12_DFTB3D3H5_cij__1__10

9
UREAXX12_DFTB3D3H5_cij__1__11

10
UREAXX12_DFTB3D3H5_cij__1__12

11
xx component is non-zero
yz component is non-zero


Cij (gradient)          :     1.9968164779689321
Error in Cij            :     7.6478141597459555
Intercept               :     0.20420975212022432
Correlation coefficient :     0.08228591968151276      <----- WARNING
calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0


Cij (gradient)          :     8.707046610822138
Error in Cij            :     6.470035511507367
Intercept               :     0.1781438207901748
Correlation coefficient :     0.3915796435039953      <----- WARNING
calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0


Cij (gradient)          :     -1.9915590108742356
Error in Cij            :     2.2276419635522076
Intercept               :     0.06373963861127678
Correlation coefficient :     -0.2720511166130249      <----- WARNING
calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0


Cij (gradient)          :     0.0029837883271152704
Error in Cij            :     0.009432952488418706
Intercept               :     -0.00028698482492120036
Correlation coefficient :     0.09953102685348861      <----- WARNING
calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0


Cij (gradient)          :     10.215581818775519
Error in Cij            :     0.10524049705212422
Intercept               :     0.004051157382039279
Correlation coefficient :     0.9994697702981931
calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0
finished pattern

Analysing pattern 2 :
UREAXX12_DFTB3D3H5_cij__2__1

0
UREAXX12_DFTB3D3H5_cij__2__2

1
UREAXX12_DFTB3D3H5_cij__2__3

2
UREAXX12_DFTB3D3H5_cij__2__4

3
UREAXX12_DFTB3D3H5_cij__2__5

4
UREAXX12_DFTB3D3H5_cij__2__6

5
UREAXX12_DFTB3D3H5_cij__2__7

6
UREAXX12_DFTB3D3H5_cij__2__8

7
UREAXX12_DFTB3D3H5_cij__2__9

8
UREAXX12_DFTB3D3H5_cij__2__10

9
UREAXX12_DFTB3D3H5_cij__2__11

10
UREAXX12_DFTB3D3H5_cij__2__12

11
zz component is non-zero
xy component is non-zero


Cij (gradient)          :     115.86802015511701
Error in Cij            :     0.3467265022933961
Intercept               :     -0.012874172572434913
Correlation coefficient :     0.9999552299331207
calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1


Cij (gradient)          :     41.367876771428946
Error in Cij            :     0.11778556647918016
Intercept               :     -0.00400815463136341
Correlation coefficient :     0.9999594676963904
calc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
finished pattern
Lower-symmetry tetragonal (4,-4,4/m)

<>---------------------------- RESULTS ----------------------------------<>

Final Cij matrix (GPa):
[[  1.99681648   8.70704661  -1.99155901   0.           0.           0.00298379]
 [  8.70704661   1.99681648  -1.99155901   0.           0.          -0.00298379]
 [ -1.99155901  -1.99155901 115.86802016   0.           0.           0.        ]
 [  0.           0.           0.          10.21558182   0.           0.        ]
 [  0.           0.           0.           0.          10.21558182   0.        ]
 [  0.00298379  -0.00298379   0.           0.           0.          41.36787677]]

Errors on Cij matrix (GPa):
[[7.64781416 6.47003551 2.22764196 0.         0.         0.00943295]
 [6.47003551 7.64781416 2.22764196 0.         0.         0.00943295]
 [2.22764196 2.22764196 0.3467265  0.         0.         0.        ]
 [0.         0.         0.         0.1052405  0.         0.        ]
 [0.         0.         0.         0.         0.1052405  0.        ]
 [0.00943295 0.00943295 0.         0.         0.         0.11778557]]

Final Sij matrix (GPa-1):
[[-0.02750028  0.12152589  0.00161613  0.          0.          0.00001075]
 [ 0.12152589 -0.02750028  0.00161613  0.          0.         -0.00001075]
 [ 0.00161613  0.00161613  0.00868607  0.          0.         -0.        ]
 [-0.          0.          0.          0.09788968 -0.         -0.        ]
 [ 0.          0.          0.          0.          0.09788968  0.        ]
 [ 0.00001075 -0.00001075 -0.          0.          0.          0.02417334]]

Errors on Sij matrix (GPa-1):
[[0.11715796 0.10228021 0.00314351 0.         0.         0.00003142]
 [0.10228021 0.11715796 0.00314351 0.         0.         0.00003142]
 [0.00314351 0.00314351 0.00007723 0.         0.         0.00000065]
 [0.         0.         0.         0.00100846 0.         0.        ]
 [0.         0.         0.         0.         0.00100846 0.        ]
 [0.00003142 0.00003142 0.00000065 0.         0.         0.00006883]]

<>----------------------------------------------------------------------<>

  Universal anisotropy index : -3.24363 +/- 2.75870
  (Rangnthn and Ostoja-Starzewski, PRL 101, 055504)


                          x           y           z
   Young's Modulus :   -36.36326   -36.36326   115.12692    GPa
         +/-       :   154.91642   154.91642     1.02367 

                        xy       xz       yx       yz       zx       zy
  Poisson's Ratios :  4.41908  0.05877  4.41908  0.05877  -0.18606  -0.18606
               +/- :  19.17121  0.27524  19.17121  0.27524  -0.36191  -0.36191

<>--------------------- POLYCRYSTALLINE RESULTS -------------------------<>

                     Voigt         +/-       Reuss         +/-       Hill          +/-
    Bulk Modulus :    14.36772     1.65055     4.92122     4.41757     9.64447     3.03406    GPa
   Shear Modulus :    20.03566     0.86720  -613.93614 14927.00530  -296.95024  7463.93625    GPa

<>-----------------------------------------------------------------------<>

